/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance software development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */
import 'package:app/views/screens/auth/auth.screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/services.dart' show SystemChannels;

import '../store/app.store.dart';
import 'theme.dart';

class App extends StatelessWidget {
  final AppStore store;

  const App(this.store);

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: this.store,
      child: GestureDetector(
        onTap: () {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
        },
        child: MaterialApp(
          theme: appTheme(context),
          debugShowCheckedModeBanner: false,
          home: AuthScreen(),
        ),
      ),
    );
  }
}
