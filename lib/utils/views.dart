/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:app/views/components/bars.dart';
import 'package:flutter/widgets.dart';

import 'async_state.dart';

Widget pickAsyncStateWidget<V>(AsyncState<V> state,
    {Widget onValue(V value),
    Widget onEmpty(),
    Widget onNotEmpty(V value),
    Widget onSuccess(V value),
    Widget onFail(dynamic error),
    Widget onProgress(),
    Widget orElse()}) {
  if (state != null) {
    if (onEmpty != null && state.value != null && (state.value as List).isEmpty)
      return onEmpty();
    if (onNotEmpty != null &&
        state.value != null &&
        (state.value as List).isNotEmpty) return onNotEmpty(state.value);
    if (onValue != null && state.value != null) return onValue(state.value);
    if (onSuccess != null && state.isSuccessful) return onSuccess(state.value);
    if (onProgress != null && state.isInProgress) return onProgress();
    if (onFail != null && state.isFailed) return onFail(state.error);
  }
  if (orElse != null) return orElse();
  return Container();
}
