/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:app/actions/auth/login.action.dart';
import 'package:app/actions/domain/load-my-gigs.action.dart';
import 'package:app/models/auth.models.dart';
import 'package:app/models/domain.models.dart';
import 'package:app/utils/async_state.dart';
import 'package:app/utils/lists.dart';
import 'package:app/utils/views.dart';
import 'package:app/views/components/domain/gig-list-item.dart';
import 'package:app/views/components/notifications.dart';
import 'package:app/views/screens/main/brand/create-gig.screen.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:redux/redux.dart';
import 'package:flutter/material.dart';

import 'package:app/store/app.store.dart';
import 'package:app/views/components/background.dart';
import 'package:app/views/components/bars.dart';

import '../../../theme.dart';
import 'manage-gig.screen.dart';

class MyGigsTab extends BottomBarTab {
  final Function redrawTopBar;
  _ViewModel _model;

  MyGigsTab(this.redrawTopBar);

  Widget icon() => Icon(FontAwesomeIcons.briefcase);

  @override
  Widget text() => Text('My Gigs');

  @override
  Widget topBar(BuildContext context, TopBar base) {
    return base.copyWith(
      leftAction: TopBarAction.add(() => _createGig(context)),
      progress: _model != null && _model.loadMyGigsState.isInProgress,
    );
  }

  void _modelInit(_ViewModel model) {
    _model = model;
    if (model.loadMyGigsState.isUseless) model.loadMyGigs();
  }

  void _modelDidChange(BuildContext context, _ViewModel model) {
    _model = model;
    if (model.hasLoadMyGigsStateChanged) redrawTopBar();
    if (model.hasLoadGigsFailed) {
      Notifications.showAsyncError(context, model.loadMyGigsState.error);
    }
  }

  void _createGig(BuildContext context) {
    Navigator.push(
        context, AppRouteTransitions.local((_) => CreateGigScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(store),
      onInitialBuild: _modelInit,
      onDidChange: (model) => _modelDidChange(context, model),
      builder: (context, model) => _body(context, model),
    );
  }

  Widget _body(BuildContext context, _ViewModel model) {
    return RefreshIndicator(
      onRefresh: () =>
          Future.delayed(Duration(milliseconds: 300), model.loadMyGigs),
      child: pickAsyncStateWidget<List<Gig>>(model.loadMyGigsState,
          onFail: (error) =>
              BackPrint.fromAsyncError(error, onTryAgain: model.loadMyGigs),
          onEmpty: () => ListView(
                children: <Widget>[
                  SizedBox(
                    height: MediaQuery.of(context).size.height - 75,
                    child: BackPrint(
                      icon: FontAwesomeIcons.exclamationCircle,
                      title: "You don't have gigs",
                      buttonTitle: "Create Gig",
                      onButtonPress: () => _createGig(context),
                    ),
                  ),
                ],
              ),
          onValue: (data) => _gigList(data)),
    );
  }

  ListView _gigList(List<Gig> gigs) {
    final open = gigs
        .where((g) => g.status == GigStatus.OPEN)
        .map((g) => ListValueItem(g));
    final started = gigs
        .where((g) => g.status == GigStatus.STARTED)
        .map((g) => ListValueItem(g));
    final closed = gigs
        .where((g) => g.status == GigStatus.CLOSED)
        .map((g) => ListValueItem(g));
    final cancelled = gigs
        .where((g) => g.status == GigStatus.CANCELLED)
        .map((g) => ListValueItem(g));
    final data = List();
    if (open.isNotEmpty) {
      data.add(ListHeaderItem('OPEN', tag: AppColors.success.withAlpha(210)));
      data.addAll(open);
    }
    if (started.isNotEmpty) {
      data.add(
          ListHeaderItem('IN PROGRESS', tag: AppColors.action.withAlpha(210)));
      data.addAll(started);
    }
    if (closed.isNotEmpty) {
      data.add(ListHeaderItem('CLOSED', tag: Colors.grey));
      data.addAll(closed);
    }
    if (cancelled.isNotEmpty) {
      data.add(
          ListHeaderItem('CANCELLED', tag: AppColors.danger.withAlpha(210)));
      data.addAll(cancelled);
    }
    return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          final item = data[index];
          if (item is ListHeaderItem) {
            return Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(5.0),
              color: item.tag,
              child: Text(
                item.header,
                style: AppStyles.header.copyWith(
                  fontSize: 16,
                  color: Colors.white,
                ),
              ),
            );
          }
          final gig = (item as ListValueItem).value;
          return InkWell(
            child: GigListItem(gig),
            onTap: () => Navigator.push(context,
                AppRouteTransitions.local((_) => ManageGigScreen(gig))),
          );
        });
  }
}

class _ViewModel {
  final AppState _state;
  final Function _dispatch;

  _ViewModel(Store<AppState> store)
      : _state = store.state,
        _dispatch = store.dispatch;

  AsyncState<Session> get loginState => _state.authState.loginState;
  AsyncState<List<Gig>> get loadMyGigsState =>
      _state.domainState.loadMyGigsState;

  void logout() => _dispatch(Logout());
  void loadMyGigs() => _dispatch(new LoadMyGigs());

  bool get hasLoadMyGigsStateChanged {
    final prev = _state.prevState?.domainState?.loadMyGigsState;
    return prev != _state.domainState.loadMyGigsState;
  }

  bool get hasLoadGigsFailed {
    final prev = _state.prevState?.domainState?.loadMyGigsState;
    return prev != _state.domainState.loadMyGigsState &&
        _state.domainState.loadMyGigsState.isFailed;
  }

  operator ==(o) {
    return o is _ViewModel &&
        this.loginState == o.loginState &&
        this.loadMyGigsState == o.loadMyGigsState;
  }

  @override
  int get hashCode => 0;
}
