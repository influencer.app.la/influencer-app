/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */
import 'package:app/actions/auth/login.action.dart';
import 'package:app/actions/domain/influencer/load-open-gigs.action.dart';
import 'package:app/models/auth.models.dart';
import 'package:app/models/domain.models.dart';
import 'package:app/utils/async_state.dart';
import 'package:app/utils/views.dart';
import 'package:app/views/components/domain/gig-list-item.dart';
import 'package:app/views/components/notifications.dart';
import 'package:app/views/theme.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:flutter/material.dart';

import 'package:app/store/app.store.dart';
import 'package:app/views/components/background.dart';
import 'package:app/views/components/bars.dart';

import 'gig.screen.dart';

class OpenGigListScreen extends StatefulWidget {
  @override
  _OpenGigListScreenState createState() => _OpenGigListScreenState();
}

class _OpenGigListScreenState extends State<OpenGigListScreen> {
  void _modelInit(_ViewModel model) {
    if (model.loadState.isUseless) model.load();
  }

  void _modelDidChange(_ViewModel model) {
    if (model.hasLoadGigsFailed) {
      Notifications.showAsyncError(context, model.loadState.error);
    }
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(store),
      onInitialBuild: _modelInit,
      onDidChange: _modelDidChange,
      builder: (context, model) {
        return Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: _appbar(model),
          body: _body(context, model),
        );
      },
    );
  }

  Widget _body(BuildContext context, _ViewModel model) {
    return pickAsyncStateWidget<List<Gig>>(
      model.loadState,
      onFail: (error) =>
          BackPrint.fromAsyncError(error, onTryAgain: model.load),
      onEmpty: () => BackPrint(
        icon: Icons.search,
        message: "No open gig found",
        buttonTitle: 'Refresh',
        onButtonPress: model.load,
      ),
      onValue: (data) => RefreshIndicator(
        onRefresh: () =>
            Future.delayed(Duration(milliseconds: 350), model.load),
        child: ListView.builder(
            itemCount: data.length,
            itemBuilder: (context, idx) {
              final gig = data[idx];
              final applied = gig.applicants.indexWhere(
                      (a) => a.id == model.loginState.value?.profile?.id) >=
                  0;
              return InkWell(
                child: GigListItem(gig, applied: applied),
                onTap: () => Navigator.push(
                    context, AppRouteTransitions.local((_) => GigScreen(gig))),
              );
            }),
      ),
    );
  }

  TopBar _appbar(_ViewModel model) {
    return TopBar(
      title: 'Open Gigs',
      progress: model.loadState.isInProgress,
      leftAction: TopBarAction.back(() => Navigator.pop(context)),
    );
  }
}

class _ViewModel {
  final AppState _state;
  final Function _dispatch;

  _ViewModel(Store<AppState> store)
      : _state = store.state,
        _dispatch = store.dispatch;

  AsyncState<Session> get loginState => _state.authState.loginState;
  AsyncState<List<Gig>> get loadState => _state.domainState.loadOpenGigsState;

  void load() => _dispatch(new LoadOpenGigs());

  bool get hasLoadGigsFailed {
    final prev = _state.prevState?.domainState?.loadOpenGigsState;
    return prev != _state.domainState.loadOpenGigsState &&
        _state.domainState.loadOpenGigsState.isFailed;
  }

  operator ==(o) {
    return o is _ViewModel && this.loadState == o.loadState;
  }

  @override
  int get hashCode => 0;
}
