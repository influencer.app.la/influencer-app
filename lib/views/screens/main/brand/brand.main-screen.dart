/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */
import 'package:app/actions/auth/login.action.dart';
import 'package:app/actions/domain/influencer/load-open-gigs.action.dart';
import 'package:app/actions/domain/load-my-gigs.action.dart';
import 'package:app/models/auth.models.dart';
import 'package:app/models/domain.models.dart';
import 'package:app/utils/async_state.dart';
import 'package:app/views/screens/main/brand/my-gigs.tab.dart';
import 'package:app/views/screens/main/brand/profile.tab.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:flutter/material.dart';

import 'package:app/store/app.store.dart';
import 'package:app/views/components/bars.dart';

class BrandMainScreen extends StatefulWidget {
  @override
  _BrandMainScreenState createState() => _BrandMainScreenState();
}

class _BrandMainScreenState extends State<BrandMainScreen> {
  int _activeTabIndex = 0;
  List<BottomBarTab> _tabs;
  Function _redrawTobbar;

  @override
  void initState() {
    _redrawTobbar = () => setState(() {});
    _tabs = [
      MyGigsTab(() => _redrawTobbar()),
      ProfileTab(),
    ];
    super.initState();
  }

  @override
  void dispose() {
    _redrawTobbar = () {};
    super.dispose();
  }

  void _modelInit(_ViewModel model) {}

  void _modelChange(_ViewModel model) {}

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(store),
      onInitialBuild: _modelInit,
      onDidChange: _modelChange,
      builder: (context, model) {
        if (model.loginState.isNotSuccessful) return SizedBox();
        return Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: _tabs[_activeTabIndex].topBar(context, _topbar(model)),
          body: _tabs[_activeTabIndex].build(context),
          bottomNavigationBar: _bottomBar(model),
        );
      },
    );
  }

  TopBar _topbar(_ViewModel model) {
    return TopBar(
      title: 'Hi, ${model.loginState.value.profile.fullname}!',
      rightAction: TopBarMenuAction(
        items: [
          TopBarMenuItem('Logout', model.logout),
        ],
      ),
    );
  }

  Widget _bottomBar(_ViewModel model) {
    return BottomBar(
      tabs: _tabs,
      activeTabIndex: _activeTabIndex,
      onTap: (i) => setState(() => _activeTabIndex = i),
    );
  }
}

class _ViewModel {
  final AppState _state;
  final Function _dispatch;

  _ViewModel(Store<AppState> store)
      : _state = store.state,
        _dispatch = store.dispatch;

  AsyncState<Session> get loginState => _state.authState.loginState;

  AsyncState<List<Gig>> get loadOpenGigsState =>
      _state.domainState.loadOpenGigsState;
  AsyncState<List<Gig>> get loadMyGigsState =>
      _state.domainState.loadMyGigsState;

  logout() => _dispatch(Logout());

  void loadOpenGigs() => _dispatch(new LoadOpenGigs());
  void loadMyGigs() => _dispatch(new LoadMyGigs());

  operator ==(o) {
    return o is _ViewModel &&
        this.loginState == o.loginState &&
        this.loadOpenGigsState == o.loadOpenGigsState &&
        this.loadMyGigsState == o.loadMyGigsState;
  }

  @override
  int get hashCode => 0;
}
