/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:flutter/material.dart';

import '../theme.dart';

class LinearActivityIndicator extends StatelessWidget {
  final double height;
  final Color color;

  LinearActivityIndicator(
      {this.height = 1.5, this.color = AppColors.secondary});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      child: Theme(
          data: ThemeData(accentColor: color),
          child: LinearProgressIndicator(backgroundColor: Colors.transparent)),
    );
  }
}

class CircularActivityIndicator extends StatelessWidget {
  final double size;
  final Color color;
  final double strokeWidth;
  final double value;

  CircularActivityIndicator(
      {this.size = 20.0,
      this.color = AppColors.primary,
      this.strokeWidth = 1.5,
      this.value});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        height: size,
        width: size,
        child: Theme(
          data: ThemeData(accentColor: color),
          child: CircularProgressIndicator(
            value: value,
            strokeWidth: strokeWidth,
          ),
        ),
      ),
    );
  }
}
