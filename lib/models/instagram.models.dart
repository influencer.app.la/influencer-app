/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

class InstagramMedia {
  final String id;
  final String link;
  final String type;
  final DateTime createdAt;
  final InstagramImages images;

  InstagramMedia.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        link = map['link'],
        type = map['type'],
        createdAt = DateTime.fromMillisecondsSinceEpoch(int.parse(map['created_time'])),
        images = InstagramImages.fromMap(map['images']);

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> map = new Map<String, dynamic>();
    map['id'] = id;
    map['link'] = link;
    map['type'] = type;
    map['created_time'] = createdAt.millisecondsSinceEpoch.toString();
    map['images'] = this.images.toMap();
    return map;
  }
}

class InstagramImages {
  final InstagramImage thumbnail;
  final InstagramImage lowResolution;
  final InstagramImage standardResolution;

  InstagramImages.fromMap(Map<String, dynamic> map)
      : thumbnail = InstagramImage.fromMap(map['thumbnail']),
        lowResolution = InstagramImage.fromMap(map['low_resolution']),
        standardResolution = InstagramImage.fromMap(map['standard_resolution']);

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['thumbnail'] = this.thumbnail.toMap();
    data['low_resolution'] = this.lowResolution.toMap();
    data['standard_resolution'] = this.standardResolution.toMap();
    return data;
  }
}

class InstagramImage {
  final int width;
  final int height;
  final String url;

  InstagramImage({this.width, this.height, this.url});

  InstagramImage.fromMap(Map<String, dynamic> json)
      : this(
          width: json['width'],
          height: json['height'],
          url: json['url'],
        );

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['width'] = this.width;
    data['height'] = this.height;
    data['url'] = this.url;
    return data;
  }
}

class InstagramProfile {
  final String id;
  final String username;
  final String fullname;
  final String picture;
  final String bio;
  final String website;
  final int mediaCount;
  final int followers;
  final int follows;
  final List<InstagramMedia> recentMedia;

  InstagramProfile.fromMap(Map map)
      : id = map['id'],
        username = map['username'],
        fullname = map['full_name'],
        picture = map['profile_picture'],
        bio = map['bio'],
        website = map['website'],
        mediaCount = map['counts']['media'],
        followers = map['counts']['followed_by'],
        follows = map['counts']['follows'],
        recentMedia = (map['recent_media'] as List)
            .map((item) => InstagramMedia.fromMap(item))
            .toList();

  Map<String, dynamic> toMap() => {
        'id': id,
        'username': username,
        'full_name': fullname,
        'profile_picture': picture,
        'bio': bio,
        'website': website,
        'counts': {
          'media': mediaCount,
          'followed_by': followers,
          'follows': follows,
        },
        'recent_media': recentMedia.map((item) => item.toMap()).toList(),
      };
}
