/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:device_info/device_info.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';

E orNull<E>() => null;

String enumToString(Object o, {bool capitalCase = false}) {
  final s = o.toString().substring('${o.runtimeType}.'.length);
  return capitalCase ? toBeginningOfSentenceCase(s.toLowerCase()) : s;
}

Future<String> getDeviceId() async {
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  if (defaultTargetPlatform == TargetPlatform.iOS) {
    IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
    return iosDeviceInfo.identifierForVendor;
  } else {
    AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
    return androidDeviceInfo.androidId;
  }
}

class PushMessage {
  final Map<String, dynamic> payload;

  PushMessage(this.payload);

  String get title => notification['title'];
  String get body => notification['body'];

  Map<dynamic, dynamic> get notification => payload['aps'] != null
      ? payload['aps']['alert']
      : payload['notification'];

  Map<dynamic, dynamic> get data =>
      defaultTargetPlatform == TargetPlatform.android
          ? payload['data']
          : payload;

  String get code => this.data['code'];

  @override
  String toString() => '$payload';
}

class Subscription {
  final Function _onStop;
  bool _active = true;

  Subscription(this._onStop);

  bool get active => _active;

  void stop() {
    _onStop();
    _active = false;
  }
}
