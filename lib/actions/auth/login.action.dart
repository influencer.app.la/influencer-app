/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */
import 'package:app/actions/api.dart';
import 'package:app/models/domain.models.dart';
import 'package:app/store/auth.store.dart';
import 'package:app/utils/async_state.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'package:app/models/auth.models.dart';
import 'package:app/store/app.store.dart';

import '../actions.dart';

class LoginStateChanged {
  final AsyncState<Session> state;

  LoginStateChanged(this.state);

  @override
  String toString() =>
      '$runtimeType.' +
      '${state.status}'.substring('.${state.status.runtimeType}'.length);
}

class Login implements AsyncAction {
  final String email;
  final String password;

  Login(this.email, this.password);

  @override
  ThunkAction<AppState> execute(api) => (store) async {
        print('login.execute()');

        store.dispatch(LoginStateChanged(AsyncState.inProgress()));
        try {
          final body = {'identifier': email, 'password': password};
          final map = await api.call('/auth/local',
              method: HttpMethod.POST, body: body);
          var session =
              Session.fromMap(map, () => store.dispatch(new Logout()));

          final profileMap = await api.call('/profiles/self', session: session);
          if (profileMap != null && profileMap['id'] != null)
            session = session.copyWith(profile: Profile.fromJson(profileMap));

          store.dispatch(LoginStateChanged(AsyncState.success(session)));
        } catch (error) {
          store.dispatch(LoginStateChanged(AsyncState.failed(error)));
        }
      };
}

class Logout implements AsyncAction {
  @override
  ThunkAction<AppState> execute(api) => (store) async {
        print('Logout.execute()');
        store.dispatch(LoginStateChanged(AsyncState.empty()));
      };
}

class CheckProfileCreated implements AsyncAction {
  @override
  ThunkAction<AppState> execute(api) => (store) async {
        print('CheckProfileCreated.execute()');

        final session = store.state.authState.loginState.value;
        if (session == null) return;

        store.dispatch(SetCheckProfileCreatedState(AsyncState.inProgress()));
        try {
          final map = await api.call('/profiles/self', session: session);
          if (map != null && map['id'] != null) {
            final entity = Profile.fromJson(map);
            store.dispatch(
                SetCheckProfileCreatedState(AsyncState.success(entity)));
          } else {
            store.dispatch(
                SetCheckProfileCreatedState(AsyncState.success(null)));
          }
        } catch (error) {
          store.dispatch(SetCheckProfileCreatedState(AsyncState.failed(error)));
        }
      };
}
