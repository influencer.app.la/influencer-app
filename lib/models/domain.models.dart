/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance software development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:app/c.dart';
import 'package:app/utils/app.dart';

import 'instagram.models.dart';

enum ProfileType { INFLUENCER, BRAND, SPONSOR, NON_PROFIT }

class Profile {
  final String id;
  final ProfileType type;
  final String fullname;
  final InstagramProfile ig;

  Profile({this.id, this.type, this.fullname, this.ig});

  factory Profile.fromJson(dynamic json) {
    if (json is String) return Profile(id: json);
    return Profile(
      id: json['id'],
      type:
          ProfileType.values.firstWhere((v) => enumToString(v) == json['type']),
      fullname: json['fullname'],
      ig: json['ig'] == null ? null : InstagramProfile.fromMap(json['ig']),
    );
  }

  Map<String, dynamic> toMap() {
    final map = <String, dynamic>{
      'id': id,
      'type': type.toString().substring('${type.runtimeType}.'.length),
      'fullname': fullname,
    };
    if (ig != null) {
      map['ig'] = ig.toMap();
    }
    return map;
  }

  Profile copyWith({
    String id,
    ProfileType type,
    String fullname,
  }) =>
      Profile(
        id: id ?? this.id,
        type: type ?? this.type,
        fullname: fullname ?? this.fullname,
        ig: ig,
      );
}

class Location {
  final String id;
  final String name;
  final String address;
  final double latitude;
  final double longitude;

  Location({
    this.id,
    this.name,
    this.address,
    this.latitude,
    this.longitude,
  });

  factory Location.fromJson(dynamic json) {
    if (json is String) return Location(id: json);
    return Location(
      id: json['id'],
      name: json['name'],
      address: json['address'],
      latitude: (json['latitude'] as num).toDouble(),
      longitude: (json['longitude'] as num).toDouble(),
    );
  }

  Location copyWith({String name}) => Location(
        id: id,
        name: name ?? this.name,
        address: address,
        latitude: latitude,
        longitude: longitude,
      );
}

enum GigStatus { OPEN, STARTED, CLOSED, CANCELLED }

class Gig {
  final String id;
  final GigStatus status;
  final String name;
  final RemoteImage photo;
  final DateTime datetime;
  final Location location;
  final Profile owner;
  final List<Profile> applicants;
  final List<Profile> hires;
  final List<Feedback> feedbacks;

  Gig({
    this.id,
    this.status,
    this.name,
    this.photo,
    this.datetime,
    this.location,
    this.owner,
    this.applicants,
    this.hires,
    this.feedbacks,
  });

  factory Gig.fromJson(dynamic json) {
    if (json is String) return Gig(id: json);
    return Gig(
      id: json['id'],
      name: json['name'],
      photo: RemoteImage.fromMap(json['photo']),
      datetime: DateTime.parse(json['datetime']),
      location: Location.fromJson(json['location']),
      owner: Profile.fromJson(json['owner']),
      applicants:
          (json['applicants'] as List).map((m) => Profile.fromJson(m)).toList(),
      hires: (json['hires'] as List).map((m) => Profile.fromJson(m)).toList(),
      status:
          GigStatus.values.firstWhere((v) => enumToString(v) == json['status']),
      feedbacks: (json['feedbacks'] as List ?? [])
          .map((item) => Feedback.fromMap(item))
          .toList(),
    );
  }

  Gig copyWith({
    String id,
    String name,
    RemoteImage photo,
    DateTime datetime,
    Location location,
    Profile owner,
    List<Profile> applicants,
    List<Profile> hires,
    GigStatus status,
    List<Feedback> feedbacks,
  }) =>
      Gig(
        id: id ?? this.id,
        name: name ?? this.name,
        photo: photo ?? this.photo,
        datetime: datetime ?? this.datetime,
        location: location ?? this.location,
        owner: owner ?? this.owner,
        applicants: applicants ?? this.applicants,
        hires: hires ?? this.hires,
        status: status ?? this.status,
        feedbacks: feedbacks ?? this.feedbacks,
      );
}

class RemoteImage {
  final String mime;
  final String url;

  RemoteImage.fromMap(Map map)
      : mime = map['mime'],
        url = map['url'];

  String get uri => '$API_SCHEMA://$API_HOST:$API_PORT$url';
}

class Feedback {
  final String text;
  final int rating;
  final Profile writer;
  final Profile receiver;
  final Gig gig;

  Feedback(this.text, this.rating, this.writer, this.receiver, this.gig);

  Feedback.fromMap(Map map)
      : text = map['text'],
        rating = map['rating'],
        writer = map['writer'] == null ? null : Profile.fromJson(map['writer']),
        receiver =
            map['receiver'] == null ? null : Profile.fromJson(map['receiver']),
        gig = map['gig'] == null ? null : Gig.fromJson(map['gig']);

  Feedback copyWith({String text, int rating}) => Feedback(
        text ?? this.text,
        rating ?? this.rating,
        this.writer,
        this.receiver,
        this.gig,
      );
}
