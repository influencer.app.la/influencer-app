/*
 * Developed in 2018 by Oleg Khalidov (brooth@gmail.com).
 *
 * Freelance Mobile Development:
 * UpWork: https://www.upwork.com/fl/khalidovoleg
 */

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../theme.dart';
import 'base.dart';

class SimpleForm extends StatelessWidget {
  final List<Widget> children;
  final double keyboardSpaceRatio;
  final ScrollController scrollController;

  SimpleForm({
    this.children,
    this.keyboardSpaceRatio = .0,
    this.scrollController,
  });

  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: ClampingScrollPhysics(),
      shrinkWrap: true,
      controller: scrollController,
      children: <Widget>[
        Container(
          constraints: BoxConstraints(maxWidth: 500.0),
          margin: AppDimensions.formPadding,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: children,
          ),
        ),
        if (keyboardSpaceRatio > .0)
          KeyboardSpace(heightRatio: keyboardSpaceRatio),
      ],
    );
  }
}

class FormTextField extends StatelessWidget {
  final String label;
  final bool secret;
  final int maxLength;
  final TextInputType inputType;
  final InputFieldController controller;
  final ValueChanged<String> onChanged;
  final bool readonly;
  final bool disabled;
  final String prefixText;
  final Widget suffix;
  final bool autoFocus;
  final BoxDecoration decoration;
  final EdgeInsets margin;
  final EdgeInsets contentPadding;
  final TextCapitalization textCapitalization;
  final TextInputAction textInputAction;
  final ValueChanged<String> onAction;
  final GestureTapCallback onTap;

  FormTextField({
    this.label,
    this.secret = false,
    this.inputType = TextInputType.text,
    this.maxLength = 100,
    this.controller,
    this.onChanged,
    this.readonly = false,
    this.disabled = false,
    this.prefixText,
    this.suffix,
    this.autoFocus = false,
    this.decoration,
    this.margin = AppDimensions.formMiddleFieldMargin,
    this.contentPadding = AppDimensions.formTextFieldContentPadding,
    this.textCapitalization = TextCapitalization.none,
    this.textInputAction,
    this.onAction,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    Widget suffix = this.suffix;
    if (suffix == null) {
      final iconSize = AppStyles.formFieldText.fontSize;
      final icon = controller?.success != null
          ? Image.asset('assets/images/ic__success.png',
              width: iconSize, height: iconSize)
          : controller?.error != null
              ? Image.asset('assets/images/ic__error.png',
                  width: iconSize, height: iconSize)
              : null;
      suffix = icon == null
          ? null
          : Padding(
              padding: EdgeInsets.only(
                  left: contentPadding.left, right: contentPadding.right),
              child: icon,
            );
    }

    return Padding(
      padding: margin,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          if (label != null)
            Padding(
              //label
              padding: AppDimensions.formLabelPadding,
              child: Text(label, style: AppStyles.formFieldLabel),
            ),
          Container(
            decoration: decoration ?? AppDecorations.formFieldBox,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                if (prefixText != null)
                  Container(
                    padding: contentPadding,
                    child: Text(prefixText, style: AppStyles.formFieldText),
                    decoration: BoxDecoration(
                      border: Border(
                        right: BorderSide(color: AppColors.formFieldBoxBorder),
                      ),
                    ),
                  ),
                Expanded(
                  child: TextField(
                    controller: controller?.editingController,
                    maxLines: 1,
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(maxLength)
                    ],
                    obscureText: secret,
                    autofocus: autoFocus,
                    textCapitalization: textCapitalization,
                    keyboardType: inputType,
                    textInputAction: textInputAction,
                    style: disabled
                        ? AppStyles.formFieldTextDisabled
                        : AppStyles.formFieldText,
                    decoration: InputDecoration(
                      enabled: !readonly && !disabled,
                      // labelText: label,
                      // labelStyle: AppStyles.authTextInputLabel,
                      // prefixText: prefixText,
                      // suffixIcon: suffix,
                      border: InputBorder.none,
                      contentPadding: contentPadding,
                    ),
                    onChanged: onChanged,
                    onSubmitted: onAction,
                    onTap: onTap,
                  ),
                ),
                if (suffix != null) suffix,
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class FormPickerField extends StatelessWidget {
  final String label;
  final String value;
  final double height;
  final bool disabled;
  final String prefixText;
  final Widget suffix;
  final BoxDecoration decoration;
  final EdgeInsets margin;
  final EdgeInsets contentPadding;
  final GestureTapCallback onTap;

  FormPickerField({
    @required this.label,
    @required this.value,
    this.height = AppDimensions.formPickerFieldHeight,
    this.disabled = false,
    this.prefixText,
    this.suffix,
    this.decoration,
    this.margin = AppDimensions.formMiddleFieldMargin,
    this.contentPadding = AppDimensions.formTextFieldContentPadding,
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    Widget suffix = this.suffix;
    if (suffix == null) {
      suffix = Padding(
        padding: const EdgeInsets.all(8.0),
        child: Icon(Icons.arrow_downward, color: Colors.grey, size: 20),
      );
    }

    return Padding(
      padding: margin,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            //label
            padding: AppDimensions.formLabelPadding,
            child: Text(label, style: AppStyles.formFieldLabel),
          ),
          Container(
            decoration: decoration ?? AppDecorations.formFieldBox,
            child: Material(
              color: Colors.transparent,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  if (prefixText != null)
                    Container(
                      padding: contentPadding,
                      child: Text(prefixText, style: AppStyles.formFieldText),
                      decoration: BoxDecoration(
                        border: Border(
                          right:
                              BorderSide(color: AppColors.formFieldBoxBorder),
                        ),
                      ),
                    ),
                  Expanded(
                    child: InkWell(
                      borderRadius: (decoration ?? AppDecorations.formFieldBox)
                          .borderRadius,
                      child: Container(
                        height: height,
                        alignment: Alignment.centerLeft,
                        padding: contentPadding,
                        child: Text(
                          value,
                          maxLines: 1,
                          style: disabled
                              ? AppStyles.formFieldTextDisabled
                              : AppStyles.formFieldText,
                        ),
                      ),
                      onTap: disabled ? null : onTap,
                    ),
                  ),
                  if (suffix != null) suffix,
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class FormImagePickerField extends StatelessWidget {
  final String label;
  final File value;
  final double height;
  final bool disabled;
  final BoxDecoration decoration;
  final EdgeInsets margin;
  final GestureTapCallback onTap;

  FormImagePickerField({
    @required this.label,
    @required this.value,
    this.height = AppDimensions.formImagePickerFieldHeight,
    this.disabled = false,
    this.decoration,
    this.margin = AppDimensions.formMiddleFieldMargin,
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    final decoration = this.decoration ?? AppDecorations.formImageFieldBox;
    return Padding(
      padding: margin,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            //label
            padding: AppDimensions.formLabelPadding,
            child: Text(label, style: AppStyles.formFieldLabel),
          ),
          InkWell(
            onTap: onTap,
            borderRadius: decoration.borderRadius,
            child: Container(
                height: height,
                decoration: decoration.copyWith(
                    image: value == null
                        ? null
                        : DecorationImage(
                            image: FileImage(value),
                            fit: BoxFit.cover,
                          )),
                child: Center(
                  child: value != null
                      ? null
                      : Icon(FontAwesomeIcons.image, color: Colors.grey),
                )),
          ),
        ],
      ),
    );
  }
}

class FormButton extends StatelessWidget {
  final String text;
  final GestureTapCallback onPressed;
  final bool progress;
  final bool disabled;
  final Color color;
  final Color disabledColor;
  final EdgeInsets margin;

  const FormButton({
    this.text,
    this.onPressed,
    this.progress = false,
    this.disabled = false,
    this.color = AppColors.formButton,
    this.disabledColor = AppColors.formButtonDisabled,
    this.margin = AppDimensions.formMiddleButtonMargin,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: margin,
      decoration: AppDecorations.formButtonBox,
      child: FlatButton(
        color: color,
        disabledColor: progress ? color : disabledColor,
        shape: RoundedRectangleBorder(
            borderRadius:
                BorderRadius.circular(AppDimensions.formButtonRadius)),
        padding: AppDimensions.formButtonContentPadding,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(text, maxLines: 1, style: AppStyles.formButtonText),
            progress
                ? Container(
                    height: 14.0,
                    width: 24.0,
                    padding: EdgeInsets.only(left: 10.0),
                    child: Theme(
                      data: ThemeData(accentColor: Colors.white),
                      child: CircularProgressIndicator(
                        strokeWidth: 1.3,
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
        onPressed: disabled ? null : onPressed,
      ),
    );
  }
}

class FormHyperlink extends StatelessWidget {
  // final Widget child;
  final List<TextSpan> text;
  final GestureTapCallback onTap;

  FormHyperlink(this.text, this.onTap);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkResponse(
        radius: 20.0,
        splashColor: Colors.black.withAlpha(30),
        containedInkWell: true,
        highlightColor: Colors.transparent,
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: RichText(
            text: TextSpan(
              children: text,
              style: AppStyles.formHyperlink,
            ),
          ),
        ),
        onTap: onTap,
      ),
    );
  }
}
