/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance software development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'dart:math';

import 'package:app/actions/auth/signup.action.dart';
import 'package:app/views/screens/auth/create-profile.screen.dart';
import 'package:app/views/screens/main/main.screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:redux/redux.dart';

import 'package:app/actions/app.actions.dart';
import 'package:app/actions/auth/login.action.dart';
import 'package:app/models/auth.models.dart';
import 'package:app/utils/async_state.dart';
import 'package:app/utils/validators.dart';
import 'package:app/views/components/form.dart';
import 'package:app/views/components/background.dart';
import 'package:app/views/components/base.dart';
import 'package:app/views/components/notifications.dart';

import '../../../c.dart';
import '../../../store/app.store.dart';
import '../../theme.dart';

class AuthScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AuthScreenState();
}

enum _Mode { SPLASH, LOGIN, SIGNUP }

class _AuthScreenState extends State<AuthScreen>
    with TickerProviderStateMixin<AuthScreen> {
  // controllers
  final _emailInputController = InputFieldController(
      validator: validateEmail, requiredError: 'Email required');
  final _passwordInputController = InputFieldController(
      validator: validatePassword, requiredError: 'Password required');
  final _passwordAgainInputController = InputFieldController();
  // animation
  AnimationController _modeAnimationController;
  Animation _modeAnimation;
  _Mode _mode;

  @override
  void initState() {
    _modeAnimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 250))
          ..addListener(() => setState(() {}));
    _modeAnimation = CurvedAnimation(
        curve: Curves.easeInOut, parent: _modeAnimationController);

    if (DEV_MODE) {
      _emailInputController.value = 'brooth@gmail.com';
      _passwordInputController.value = 'password';
      _passwordAgainInputController.value = 'password';
    }

    super.initState();
  }

  void dispose() {
    _modeAnimationController.dispose();
    super.dispose();
  }

  _changeModeWithFormAnimation(_Mode next,
      {int forwardDelay = 0, double reverseFrom, double forwardFrom}) {
    _modeAnimationController.reverse(from: reverseFrom).asStream().listen((_) {
      setState(() {
        _mode = next;
        _emailInputController.error = null;
        _passwordInputController.error = null;
        _passwordAgainInputController.error = null;
      });
      Future.delayed(Duration(milliseconds: forwardDelay)).then((_) {
        try {
          _modeAnimationController.forward(from: forwardFrom);
        } catch (e) {/* called after mismissed error */}
      });
    });
  }

  _initialState(_ViewModel model) {
    if (model.initAppState.isUseless) {
      if (_mode != _Mode.SPLASH) _changeModeWithFormAnimation(_Mode.SPLASH);
      model.initApp();
    } else {
      _changeModeWithFormAnimation(_Mode.LOGIN);
    }
  }

  _stateChanged(_ViewModel model) {
    if (model.loginState.isSuccessful) {
      if (model.loginState.value.profile == null) {
        Navigator.pushReplacement(
            context, AppRouteTransitions.local((_) => CreateProfileScreen()));
      } else {
        Navigator.pushReplacement(
            context, AppRouteTransitions.fade((_) => MainScreen()));
      }
      return;
    }
    if (model.hasInitAppSucceed) {
      _changeModeWithFormAnimation(_Mode.LOGIN,
          forwardDelay: 400, reverseFrom: .0);
    }
    if (model.hasLoginFailed) {
      Notifications.showAsyncError(context, model.loginState.error);
    }
    if (model.hasSignUpFailed) {
      Notifications.showAsyncError(context, model.signUpState.error);
    }
  }

  _login(_ViewModel model) {
    String validationError = _emailInputController.validate(context) ??
        _passwordInputController.validate(context);
    if (validationError != null) {
      setState(() {
        Notifications.showDanger(context, message: validationError);
      });
      return;
    }
    model.login(_emailInputController.value, _passwordInputController.value);
  }

  _signUp(_ViewModel model) {
    if (_passwordInputController.value != _passwordAgainInputController.value) {
      _passwordAgainInputController.error = 'Passwords don\'t match';
    }
    String validationError = _emailInputController.validate(context) ??
        _passwordInputController.validate(context) ??
        _passwordAgainInputController.validate(context);
    if (validationError != null) {
      setState(() {
        Notifications.showDanger(context, message: validationError);
      });
      return;
    }
    model.signUp(_emailInputController.value, _passwordInputController.value);
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final size = mediaQuery.size;
    final keyboardSpace = mediaQuery.viewInsets.vertical;
    final formHeight = _mode == _Mode.LOGIN
        ? (mediaQuery.devicePixelRatio * 55 + 155)
        : _mode == _Mode.SIGNUP
            ? (mediaQuery.devicePixelRatio * 50 + 250)
            : 50.0;
    final fromBottomSpace =
        max(keyboardSpace + 10, size.height / 2 - formHeight / 2);
    final formTopSpace = size.height - formHeight - fromBottomSpace;
    final logoWidth = min(150.0, size.height / 5);
    var logoHeight = min(
        formTopSpace,
        _mode == null || _mode == _Mode.SPLASH
            ? size.height / 2 - logoWidth / 2
            : size.height / 3 - logoWidth / 2);
    if (logoHeight < 120) logoHeight = 10;
    return StoreConnector<AppState, _ViewModel>(
      distinct: true,
      converter: (store) => _ViewModel(store),
      onInitialBuild: _initialState,
      onDidChange: _stateChanged,
      builder: (context, model) => AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarBrightness: Brightness.light,
        ),
        child: Material(
          color: AppColors.primaryBg,
          child: Stack(
            children: <Widget>[
              AuthBackground(),
              Align(
                alignment: Alignment.topCenter,
                child: AnimatedContainer(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  duration: Duration(milliseconds: 250),
                  curve: Curves.easeInOut,
                  alignment: Alignment.bottomCenter,
                  height: logoHeight,
                  width: _mode == null || _mode == _Mode.SPLASH
                      ? logoWidth
                      : logoWidth / 1.5,
                  child: Image.asset('assets/images/logo.png'),
                ),
              ),
              Positioned(
                bottom: -formHeight +
                    (formHeight * _modeAnimation.value +
                        fromBottomSpace * _modeAnimation.value),
                width: size.width,
                child: SizedBox(height: formHeight, child: _form(model)),
              ),
              Positioned(
                top: size.height - (60 * _modeAnimation.value),
                width: size.width,
                child: Center(child: _modeHyperlink(model)),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _form(_ViewModel model) {
    if (_mode == _Mode.LOGIN) return _loginForm(model);
    if (_mode == _Mode.SIGNUP) return _signUpForm(model);
    return SpinKitThreeBounce(
      size: 20,
      itemBuilder: (_, index) => DecoratedBox(
        decoration: BoxDecoration(
          color: AppColors.primary,
        ),
      ),
    );
  }

  Widget _loginForm(_ViewModel model) {
    return SimpleForm(children: [
      FormTextField(
        margin: AppDimensions.formTopFieldMargin,
        label: 'Email',
        inputType: TextInputType.emailAddress,
        maxLength: 50,
        readonly: model.loginState.isInProgress,
        controller: _emailInputController,
        onChanged: (value) {
          if (_emailInputController.error != null) {
            setState(() => _emailInputController.error = null);
          }
        },
      ),
      FormTextField(
        margin: AppDimensions.formMiddleFieldMargin,
        label: 'Password',
        inputType: TextInputType.text,
        secret: true,
        maxLength: 50,
        readonly: model.loginState.isInProgress,
        controller: _passwordInputController,
        textInputAction: TextInputAction.go,
        onAction: (_) => _login(model),
        onChanged: (value) {
          if (_passwordInputController.error != null) {
            setState(() => _passwordInputController.error = null);
          }
        },
      ),
      FormButton(
        margin: AppDimensions.formTopButtonMargin,
        text: 'SIGN IN',
        onPressed: () => _login(model),
        disabled: model.loginState.isInProgress,
        progress: model.loginState.isInProgress,
      ),
    ]);
  }

  Widget _signUpForm(_ViewModel model) {
    return SimpleForm(children: [
      FormTextField(
        margin: AppDimensions.formTopFieldMargin,
        label: 'Email',
        inputType: TextInputType.emailAddress,
        maxLength: 50,
        readonly: model.signUpState.isInProgress,
        disabled: model.signUpState.isInProgress,
        controller: _emailInputController,
        onChanged: (value) {
          if (_emailInputController.error != null) {
            setState(() => _emailInputController.error = null);
          }
        },
      ),
      FormTextField(
        margin: AppDimensions.formMiddleFieldMargin,
        label: 'Password',
        inputType: TextInputType.text,
        secret: true,
        maxLength: 50,
        readonly: model.signUpState.isInProgress,
        disabled: model.signUpState.isInProgress,
        controller: _passwordInputController,
        onChanged: (value) {
          if (_passwordInputController.error != null) {
            setState(() => _passwordInputController.error = null);
          }
        },
      ),
      FormTextField(
        margin: AppDimensions.formMiddleFieldMargin,
        label: 'Password Again',
        inputType: TextInputType.text,
        secret: true,
        maxLength: 50,
        readonly: model.signUpState.isInProgress,
        disabled: model.signUpState.isInProgress,
        controller: _passwordAgainInputController,
        textInputAction: TextInputAction.go,
        onAction: (_) => _signUp(model),
        onChanged: (value) {
          if (_passwordAgainInputController.error != null) {
            setState(() => _passwordAgainInputController.error = null);
          }
        },
      ),
      FormButton(
        margin: AppDimensions.formTopButtonMargin,
        text: 'SIGN UP',
        onPressed: () => _signUp(model),
        disabled: model.signUpState.isInProgress,
        progress: model.signUpState.isInProgress,
      ),
    ]);
  }

  _modeHyperlink(_ViewModel model) {
    if (_mode == _Mode.LOGIN)
      return FormHyperlink([
        TextSpan(text: 'Don\'t have an account? '),
        TextSpan(text: 'SIGN UP', style: TextStyle(fontWeight: FontWeight.bold))
      ], () => _changeModeWithFormAnimation(_Mode.SIGNUP));
    if (_mode == _Mode.SIGNUP)
      return FormHyperlink([
        TextSpan(text: 'Already have an account? '),
        TextSpan(text: 'SIGN IN', style: TextStyle(fontWeight: FontWeight.bold))
      ], () => _changeModeWithFormAnimation(_Mode.LOGIN));
    return SizedBox();
  }
}

class _ViewModel {
  final AppState _state;
  final Function _dispatch;

  _ViewModel(Store<AppState> store)
      : _state = store.state,
        _dispatch = store.dispatch;

  AsyncState<void> get initAppState => _state.initState;
  AsyncState<Session> get loginState => _state.authState.loginState;
  AsyncState<Session> get signUpState => _state.authState.signUpState;

  bool get hasInitAppSucceed {
    final prev = _state.prevState?.initState;
    return prev != _state.initState && _state.initState.isSuccessful;
  }

  bool get hasLoginFailed {
    final prev = _state.prevState?.authState?.loginState;
    return prev != _state.authState.loginState &&
        _state.authState.loginState.isFailed;
  }

  bool get hasSignUpFailed {
    final prev = _state.prevState?.authState?.signUpState;
    return prev != _state.authState.signUpState &&
        _state.authState.signUpState.isFailed;
  }

  initApp() => _dispatch(new InitApp());

  login(String email, String password) => _dispatch(Login(email, password));

  signUp(String email, String password) => _dispatch(SignUp(email, password));

  operator ==(o) {
    return o is _ViewModel &&
        this.initAppState == o.initAppState &&
        this.loginState == o.loginState &&
        this.signUpState == o.signUpState;
  }

  @override
  int get hashCode => 0;
}
