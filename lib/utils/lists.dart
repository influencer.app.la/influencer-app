/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

class ListHeaderItem<T> {
  final String header;
  final T tag;

  ListHeaderItem(this.header, {this.tag});
}

class ListValueItem<V> {
  final V value;

  ListValueItem(this.value);
}
