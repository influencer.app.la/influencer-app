/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'dart:math';

import 'package:app/i18n/i18n.dart';
import 'package:app/utils/async_state.dart';
import 'package:app/views/theme.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../c.dart';

class Notifications {
  static showAsyncError(BuildContext context, AsyncError error) {
    String title;
    String message = I18N.fromAsyncError(error);
    if (error.cause != null && DEV_MODE) {
      title = message;
      message = error.cause.toString();
    }
    build(new Notification(
      title: title,
      message: message,
      icon: Padding(
        padding: AppDimensions.notificationIconPadding,
        child: Icon(
          FontAwesomeIcons.exclamationTriangle,
          color: (error.severity == AsyncErrorSeverity.DANGER)
              ? AppColors.danger
              : AppColors.warning,
          size: AppDimensions.notificationIconSize,
        ),
      ),
    ))
      ..show(context);
  }

  static Flushbar showSuccess(BuildContext context,
          {String title, String message}) =>
      build(new Notification(
        title: title,
        message: message,
        icon: Padding(
          padding: AppDimensions.notificationIconPadding,
          child: Icon(
            FontAwesomeIcons.exclamationCircle,
            color: AppColors.success,
            size: AppDimensions.notificationIconSize,
          ),
        ),
      ))
        ..show(context);

  static Flushbar showWarning(BuildContext context,
          {String title, String message}) =>
      build(new Notification(
        title: title,
        message: message,
        icon: Padding(
          padding: AppDimensions.notificationIconPadding,
          child: Icon(
            FontAwesomeIcons.exclamationTriangle,
            color: AppColors.warning,
            size: AppDimensions.notificationIconSize,
          ),
        ),
      ))
        ..show(context);

  static Flushbar showDanger(BuildContext context,
          {String title, String message}) =>
      build(new Notification(
        title: title,
        message: message,
        icon: Padding(
          padding: AppDimensions.notificationIconPadding,
          child: Icon(
            FontAwesomeIcons.exclamationTriangle,
            color: AppColors.danger,
            size: AppDimensions.notificationIconSize,
          ),
        ),
      ))
        ..show(context);

  static Flushbar workInProgress(BuildContext context) =>
      showWarning(context, message: 'Work In Progress...');

  static Flushbar build(Notification opts) => Flushbar(
        messageText: Container(
          padding: AppDimensions.notificationContentPadding,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                opts.titleWidget ?? opts.title != null
                    ? Text(opts.title, style: AppStyles.notificationTitle)
                    : SizedBox(),
                opts.messageWidget ?? opts.message != null
                    ? Text(opts.message, style: AppStyles.notificationMessage)
                    : SizedBox(),
              ]),
        ),
        icon: opts.icon,
        shouldIconPulse: opts.shouldIconPulse ?? true,
        margin: opts.margin ?? AppDimensions.notificationMargin,
        padding: opts.padding ?? AppDimensions.notificationPadding,
        borderRadius: opts.borderRadius ?? 0.0,
        borderColor: opts.borderColor,
        borderWidth: opts.borderWidth ?? 1.0,
        backgroundColor: opts.backgroundColor ?? AppColors.notificationBg,
        leftBarIndicatorColor: opts.leftBarIndicatorColor,
        boxShadows: opts.boxShadows ?? AppShadows.notificationBox,
        backgroundGradient: opts.backgroundGradient,
        mainButton: opts.mainButton,
        onTap: opts.onTap,
        duration: opts.duration ??
            Duration(milliseconds: max(opts.title?.length ?? 0 * 70, 2000)) +
                Duration(
                    milliseconds: max(opts.message?.length ?? 0 * 70, 2000)),
        isDismissible: opts.isDismissible ?? true,
        dismissDirection:
            opts.dismissDirection ?? FlushbarDismissDirection.VERTICAL,
        showProgressIndicator: opts.showProgressIndicator ?? false,
        progressIndicatorController: opts.progressIndicatorController,
        progressIndicatorBackgroundColor: opts.progressIndicatorBackgroundColor,
        progressIndicatorValueColor: opts.progressIndicatorValueColor,
        flushbarPosition: opts.flushbarPosition ?? FlushbarPosition.TOP,
        flushbarStyle: opts.flushbarStyle ?? FlushbarStyle.GROUNDED,
        forwardAnimationCurve: opts.forwardAnimationCurve ?? Curves.easeOut,
        reverseAnimationCurve:
            opts.reverseAnimationCurve ?? Curves.fastOutSlowIn,
        animationDuration:
            opts.animationDuration ?? Duration(milliseconds: 300),
        onStatusChanged: opts.onStatusChanged,
        overlayBlur: opts.overlayBlur ?? 0.0,
        overlayColor: opts.overlayColor ?? Colors.red,
        userInputForm: opts.userInputForm,
      );
}

class Notification {
  final String title;
  final String message;
  final Widget titleWidget;
  final Widget messageWidget;
  final Widget icon;
  final bool shouldIconPulse;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final double borderRadius;
  final Color borderColor;
  final double borderWidth;
  final Color backgroundColor;
  final Color leftBarIndicatorColor;
  final List<BoxShadow> boxShadows;
  final Gradient backgroundGradient;
  final FlatButton mainButton;
  final Function(Flushbar) onTap;
  final Duration duration;
  final bool isDismissible;
  final FlushbarDismissDirection dismissDirection;
  final bool showProgressIndicator;
  final AnimationController progressIndicatorController;
  final Color progressIndicatorBackgroundColor;
  final Animation<Color> progressIndicatorValueColor;
  final FlushbarPosition flushbarPosition;
  final FlushbarStyle flushbarStyle;
  final Curve forwardAnimationCurve;
  final Curve reverseAnimationCurve;
  final Duration animationDuration;
  final FlushbarStatusCallback onStatusChanged;
  final double overlayBlur;
  final Color overlayColor;
  final Form userInputForm;

  Notification({
    this.title,
    this.message,
    this.titleWidget,
    this.messageWidget,
    this.icon,
    this.shouldIconPulse,
    this.margin,
    this.padding,
    this.borderRadius,
    this.borderColor,
    this.borderWidth,
    this.backgroundColor,
    this.leftBarIndicatorColor,
    this.boxShadows,
    this.backgroundGradient,
    this.mainButton,
    this.onTap,
    this.duration,
    this.isDismissible,
    this.dismissDirection,
    this.showProgressIndicator,
    this.progressIndicatorController,
    this.progressIndicatorBackgroundColor,
    this.progressIndicatorValueColor,
    this.flushbarPosition,
    this.flushbarStyle,
    this.forwardAnimationCurve,
    this.reverseAnimationCurve,
    this.animationDuration,
    this.onStatusChanged,
    this.overlayBlur,
    this.overlayColor,
    this.userInputForm,
  });

  Notification copyWith({
    String title,
    String message,
    Widget titleText,
    Widget messageText,
    Widget icon,
    bool shouldIconPulse,
    EdgeInsets margin,
    EdgeInsets padding,
    double borderRadius,
    Color borderColor,
    double borderWidth,
    Color backgroundColor,
    Color leftBarIndicatorColor,
    List<BoxShadow> boxShadows,
    Gradient backgroundGradient,
    FlatButton mainButton,
    Function(Flushbar) onTap,
    Duration duration,
    bool isDismissible,
    FlushbarDismissDirection dismissDirection,
    bool showProgressIndicator,
    AnimationController progressIndicatorController,
    Color progressIndicatorBackgroundColor,
    Animation<Color> progressIndicatorValueColor,
    FlushbarPosition flushbarPosition,
    FlushbarStyle flushbarStyle,
    Curve forwardAnimationCurve,
    Curve reverseAnimationCurve,
    Duration animationDuration,
    FlushbarStatusCallback onStatusChanged,
    double overlayBlur,
    Color overlayColor,
    Form userInputForm,
  }) =>
      Notification(
        title: title ?? this.title,
        message: message ?? this.message,
        titleWidget: titleText ?? this.titleWidget,
        messageWidget: messageText ?? this.messageWidget,
        icon: icon ?? this.icon,
        shouldIconPulse: shouldIconPulse ?? this.shouldIconPulse,
        margin: margin ?? this.margin,
        padding: padding ?? this.padding,
        borderRadius: borderRadius ?? this.borderRadius,
        borderColor: borderColor ?? this.borderColor,
        borderWidth: borderWidth ?? this.borderWidth,
        backgroundColor: backgroundColor ?? this.backgroundColor,
        leftBarIndicatorColor:
            leftBarIndicatorColor ?? this.leftBarIndicatorColor,
        boxShadows: boxShadows ?? this.boxShadows,
        backgroundGradient: backgroundGradient ?? this.backgroundGradient,
        mainButton: mainButton ?? this.mainButton,
        onTap: onTap ?? this.onTap,
        duration: duration ?? this.duration,
        isDismissible: isDismissible ?? this.isDismissible,
        dismissDirection: dismissDirection ?? this.dismissDirection,
        showProgressIndicator:
            showProgressIndicator ?? this.showProgressIndicator,
        progressIndicatorController:
            progressIndicatorController ?? this.progressIndicatorController,
        progressIndicatorBackgroundColor: progressIndicatorBackgroundColor ??
            this.progressIndicatorBackgroundColor,
        progressIndicatorValueColor:
            progressIndicatorValueColor ?? this.progressIndicatorValueColor,
        flushbarPosition: flushbarPosition ?? this.flushbarPosition,
        flushbarStyle: flushbarStyle ?? this.flushbarStyle,
        forwardAnimationCurve:
            forwardAnimationCurve ?? this.forwardAnimationCurve,
        reverseAnimationCurve:
            reverseAnimationCurve ?? this.reverseAnimationCurve,
        animationDuration: animationDuration ?? this.animationDuration,
        onStatusChanged: onStatusChanged ?? this.onStatusChanged,
        overlayBlur: overlayBlur ?? this.overlayBlur,
        overlayColor: overlayColor ?? this.overlayColor,
        userInputForm: userInputForm ?? this.userInputForm,
      );
}
