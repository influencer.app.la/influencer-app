// ignore_for_file: non_constant_identifier_names

/*
 * @author Oleg Khalidov (brooth@gmail.com).
 * -----------------------------------------------
 * Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

bool get DEV_MODE {
  bool inDebugMode = false;
  assert(inDebugMode = true);
  return inDebugMode;
}

final API_SCHEMA = 'http';
final API_HOST = !DEV_MODE ? '188.93.211.121' : '192.168.0.6';
final API_PORT = 1338;

final INSTAGRAM_CLIENT_ID = 'a4c8dc63d1ac4f2e9ac405b5c56da37a';
