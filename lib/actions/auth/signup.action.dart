/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:app/actions/api.dart';
import 'package:app/models/auth.models.dart';
import 'package:app/store/app.store.dart';
import 'package:app/utils/async_state.dart';
import 'package:redux_thunk/redux_thunk.dart';

import '../actions.dart';
import 'login.action.dart';

class SignUpStateChanged {
  final AsyncState<Session> state;

  SignUpStateChanged(this.state);

  @override
  String toString() =>
      '$runtimeType.' +
      '${state.status}'.substring('.${state.status.runtimeType}'.length);
}

class SignUp implements AsyncAction {
  final String email;
  final String password;

  SignUp(this.email, this.password);

  @override
  ThunkAction<AppState> execute(api) => (store) async {
        print('SignUp.execute()');

        store.dispatch(SignUpStateChanged(AsyncState.inProgress()));
        try {
          final body = {
            'username': email,
            'email': email,
            'password': password,
          };
          final map = await api.call('/auth/local/register',
              method: HttpMethod.POST, body: body);
          final entity =
              Session.fromMap(map, () => store.dispatch(new Logout()));
          store.dispatch(SignUpStateChanged(AsyncState.success(entity)));
        } catch (error) {
          store.dispatch(SignUpStateChanged(AsyncState.failed(error)));
        }
      };
}
