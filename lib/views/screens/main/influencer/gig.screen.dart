/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:app/actions/domain/influencer/apply-for-gig.action.dart';
import 'package:app/actions/domain/influencer/quit-gig.action.dart';
import 'package:app/models/auth.models.dart';
import 'package:app/utils/async_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';

import 'package:app/models/domain.models.dart';
import 'package:app/store/app.store.dart';
import 'package:app/views/components/bars.dart';
import 'package:app/views/components/form.dart';
import 'package:app/views/components/notifications.dart';
import 'package:app/views/theme.dart';

class GigScreen extends StatefulWidget {
  final Gig gig;

  GigScreen(this.gig);

  @override
  _GigScreenState createState() => _GigScreenState();
}

class _GigScreenState extends State<GigScreen> {
  Gig _gig;
  bool _showTopbarDimmer = false;

  @override
  void initState() {
    _gig = widget.gig;
    super.initState();
  }

  _modelInit(model) {
    setState(() => _showTopbarDimmer = true);
  }

  _modelDidChange(_ViewModel model) {
    if (model.hasLoadGigsStateChanged && model.loadGigsState.isSuccessful) {
      setState(() {
        _gig = model.loadGigsState.value.firstWhere((g) => g.id == _gig.id);
      });
    }
    if (model.hasApplyForGigFailed) {
      Notifications.showAsyncError(context, model.applyForGigState.error);
    }
    if (model.hasQuitGigFailed) {
      Notifications.showAsyncError(context, model.quitGigState.error);
    }
    if (model.hasApplyForGigSucceed) {
      Notifications.showSuccess(context,
          message: "You have applied for this gig.");
    }
    if (model.hasQuitGigSucceed) {
      Notifications.showSuccess(context,
          message: "You've been excluded from the applicants.");
    }
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
        converter: (store) => _ViewModel(store),
        onInitialBuild: _modelInit,
        onDidChange: _modelDidChange,
        builder: (context, model) {
          final profile = model.loginState.value?.profile;
          if (profile == null) return SizedBox();
          return Scaffold(
            backgroundColor: Colors.white,
            body: CustomScrollView(
              physics: ClampingScrollPhysics(),
              slivers: <Widget>[
                _appBar(),
                SliverList(
                    delegate: SliverChildListDelegate(<Widget>[
                  _body(model, profile),
                ]))
              ],
            ),
          );
        });
  }

  Widget _body(_ViewModel model, Profile profile) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 25),
          Row(
            children: <Widget>[
              SizedBox(width: 2),
              Icon(
                FontAwesomeIcons.mapMarkerAlt,
                color: AppColors.secondary,
                size: 20,
              ),
              SizedBox(width: 20),
              Text(_gig.location.name,
                  style: AppStyles.primaryText.copyWith(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  )),
            ],
          ),
          SizedBox(height: 10),
          Padding(
            padding: EdgeInsets.only(left: 45),
            child: Text(
              _gig.location.address,
              style: AppStyles.secondaryText,
            ),
          ),
          SizedBox(height: 20),
          Row(
            children: <Widget>[
              SizedBox(width: 2),
              Icon(
                FontAwesomeIcons.userTie,
                color: AppColors.secondary,
                size: 20,
              ),
              SizedBox(width: 20),
              Padding(
                padding: const EdgeInsets.only(top: 4),
                child: Text(_gig.owner.fullname,
                    style: AppStyles.primaryText.copyWith(fontSize: 18)),
              ),
            ],
          ),
          SizedBox(height: 20),
          Row(
            children: <Widget>[
              SizedBox(width: 2),
              Icon(
                FontAwesomeIcons.calendarAlt,
                color: AppColors.secondary,
                size: 20,
              ),
              SizedBox(width: 20),
              Padding(
                padding: const EdgeInsets.only(top: 4),
                child: Text(DateFormat.yMd().format(_gig.datetime),
                    style: AppStyles.primaryText.copyWith(fontSize: 18)),
              ),
            ],
          ),
          SizedBox(height: 20),
          Row(
            children: <Widget>[
              Icon(
                FontAwesomeIcons.users,
                color: AppColors.secondary,
                size: 20,
              ),
              SizedBox(width: 20),
              Padding(
                padding: const EdgeInsets.only(top: 4),
                child: Text(
                    '${_gig.applicants.length} ${_gig.applicants.length == 1 ? "applicant" : "applicants"}',
                    style: AppStyles.primaryText.copyWith(fontSize: 18)),
              ),
            ],
          ),
          SizedBox(height: 20),
          if (_gig.status == GigStatus.CLOSED)
            Row(
              children: <Widget>[
                Icon(
                  FontAwesomeIcons.userFriends,
                  color: AppColors.secondary,
                  size: 20,
                ),
                SizedBox(width: 20),
                Padding(
                  padding: const EdgeInsets.only(top: 4),
                  child: Text(
                      '${_gig.hires.length} ${_gig.hires.length == 1 ? "hire" : "hires"}',
                      style: AppStyles.primaryText.copyWith(fontSize: 18)),
                ),
              ],
            )
          else
            SizedBox(height: 10),
          if (_gig.status == GigStatus.OPEN)
            if (_gig.applicants.indexWhere((a) => a.id == profile.id) == -1)
              FormButton(
                text: 'APPLY',
                onPressed: () => model.applyForGig(_gig),
                progress: model.applyForGigState.isInProgress,
                disabled: model.applyForGigState.isInProgress,
              )
            else
              FormButton(
                text: 'QUIT',
                color: AppColors.danger,
                onPressed: () => model.quitGig(_gig),
                progress: model.quitGigState.isInProgress,
                disabled: model.quitGigState.isInProgress,
              ),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  Widget _appBar() {
    final mediaQuery = MediaQuery.of(context);
    final width = mediaQuery.size.width;
    final imageHeight = (width * 3 / 4).floorToDouble();
    return SliverAppBar(
      pinned: true,
      expandedHeight: imageHeight,
      brightness: Brightness.dark,
      backgroundColor: AppColors.primary,
      title: Padding(
        padding: AppDimensions.topBarContentPadding,
        child: Text(widget.gig.name,
            style: AppStyles.topBarTitle.copyWith(color: Colors.white)),
      ),
      centerTitle: true,
      leading: TopBarAction.back(
        () => Navigator.pop(context),
        color: Colors.white,
      ),
      flexibleSpace: FlexibleSpaceBar(
        background: Stack(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              height: double.infinity,
              child: Hero(
                tag: widget.gig.id,
                child: Image.network(
                  widget.gig.photo.uri,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            AnimatedOpacity(
              duration: Duration(milliseconds: 300),
              opacity: _showTopbarDimmer ? 1 : 0,
              child: Container(
                width: double.infinity,
                height: imageHeight,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.black.withOpacity(.83),
                    Colors.transparent,
                  ],
                )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _ViewModel {
  final AppState _state;
  final Function _dispatch;

  _ViewModel(Store<AppState> store)
      : _state = store.state,
        _dispatch = store.dispatch;

  AsyncState<Session> get loginState => _state.authState.loginState;
  AsyncState<List<Gig>> get loadGigsState =>
      _state.domainState.loadOpenGigsState;
  AsyncState<Gig> get applyForGigState => _state.domainState.applyForGigState;
  AsyncState<Gig> get quitGigState => _state.domainState.quitGigState;

  void applyForGig(Gig gig) => _dispatch(new ApplyForGig(gig));
  void quitGig(Gig gig) => _dispatch(new QuitGig(gig));

  bool get hasApplyForGigSucceed {
    final prev = _state.prevState?.domainState?.applyForGigState;
    return prev != _state.domainState.applyForGigState &&
        _state.domainState.applyForGigState.isSuccessful;
  }

  bool get hasQuitGigSucceed {
    final prev = _state.prevState?.domainState?.quitGigState;
    return prev != _state.domainState.quitGigState &&
        _state.domainState.quitGigState.isSuccessful;
  }

  bool get hasApplyForGigFailed {
    final prev = _state.prevState?.domainState?.applyForGigState;
    return prev != _state.domainState.applyForGigState &&
        _state.domainState.applyForGigState.isFailed;
  }

  bool get hasQuitGigFailed {
    final prev = _state.prevState?.domainState?.quitGigState;
    return prev != _state.domainState.quitGigState &&
        _state.domainState.quitGigState.isFailed;
  }

  bool get hasLoadGigsStateChanged {
    final prev = _state.prevState?.domainState?.loadOpenGigsState;
    return prev != _state.domainState.loadOpenGigsState;
  }

  operator ==(o) {
    return o is _ViewModel &&
        loadGigsState == o.loadGigsState &&
        applyForGigState == o.applyForGigState &&
        quitGigState == o.quitGigState;
  }

  @override
  int get hashCode => 0;
}
