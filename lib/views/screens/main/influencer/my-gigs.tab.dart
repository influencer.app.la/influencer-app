/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */
import 'package:app/models/auth.models.dart';
import 'package:app/views/components/bars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:redux/redux.dart';

import 'package:app/actions/domain/load-my-gigs.action.dart';
import 'package:app/models/domain.models.dart';
import 'package:app/store/app.store.dart';
import 'package:app/utils/async_state.dart';
import 'package:app/views/components/background.dart';
import 'package:app/views/components/domain/gig-list-item.dart';
import 'package:app/views/components/notifications.dart';
import 'package:app/utils/views.dart';

import '../../../theme.dart';
import 'gig.screen.dart';

class MyGigsTab extends BottomBarTab {
  final Function redrawTopBar;
  _ViewModel _model;

  MyGigsTab(this.redrawTopBar);

  Widget icon() => Icon(FontAwesomeIcons.briefcase);

  @override
  Widget text() => Text('My Gigs');

  @override
  Widget topBar(BuildContext context, TopBar base) {
    return base.copyWith(
      progress: _model != null && _model.loadState.isInProgress,
    );
  }

  void _modelInit(_ViewModel model) {
    _model = model;
    if (model.loadState.isUseless) {
      model.load();
    }
  }

  void _modelDidChange(BuildContext context, _ViewModel model) {
    _model = model;
    if (model.hasLoadGigsStateChanged) redrawTopBar();
    if (model.hasLoadGigsFailed) {
      Notifications.showAsyncError(context, model.loadState.error);
    }
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(store),
      onInitialBuild: _modelInit,
      onDidChange: (model) => _modelDidChange(context, model),
      builder: (context, model) {
        return _body(context, model);
      },
    );
  }

  Widget _body(BuildContext context, _ViewModel model) {
    return pickAsyncStateWidget<List<Gig>>(
      model.loadState,
      onFail: (error) =>
          BackPrint.fromAsyncError(error, onTryAgain: model.load),
      onEmpty: () => BackPrint(
        icon: FontAwesomeIcons.frown,
        iconSize: 35,
        message: "You haven't been hired yet",
        buttonTitle: 'Refresh',
        onButtonPress: model.load,
      ),
      onValue: (data) => RefreshIndicator(
        onRefresh: () =>
            Future.delayed(Duration(milliseconds: 350), model.load),
        child: ListView.builder(
            itemCount: data.length,
            itemBuilder: (context, idx) {
              final gig = data[idx];
              final applied = gig.applicants.indexWhere(
                      (a) => a.id == model.loginState.value?.profile?.id) >=
                  0;
              return InkWell(
                child: GigListItem(gig, applied: applied),
                onTap: () => Navigator.push(
                    context, AppRouteTransitions.local((_) => GigScreen(gig))),
              );
            }),
      ),
    );
  }
}

class _ViewModel {
  final AppState _state;
  final Function _dispatch;

  _ViewModel(Store<AppState> store)
      : _state = store.state,
        _dispatch = store.dispatch;

  AsyncState<Session> get loginState => _state.authState.loginState;
  AsyncState<List<Gig>> get loadState => _state.domainState.loadMyGigsState;

  void load() => _dispatch(new LoadMyGigs());

  bool get hasLoadGigsStateChanged {
    final prev = _state.prevState?.domainState?.loadMyGigsState;
    return prev != _state.domainState.loadMyGigsState;
  }

  bool get hasLoadGigsFailed {
    final prev = _state.prevState?.domainState?.loadMyGigsState;
    return prev != _state.domainState.loadMyGigsState &&
        _state.domainState.loadMyGigsState.isFailed;
  }

  operator ==(o) {
    return o is _ViewModel && this.loadState == o.loadState;
  }

  @override
  int get hashCode => 0;
}
