/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance software development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

enum AsyncStatus { IDLE, IN_PROGRESS, SUCCESS, FAILED }

class AsyncState<V> {
  final AsyncStatus status;
  final V value;
  final AsyncError error;

  const AsyncState(this.status, [this.value, this.error]);

  const AsyncState.empty([V value]) : this(AsyncStatus.IDLE, value);
  const AsyncState.inProgress([V value]) : this(AsyncStatus.IN_PROGRESS, value);
  const AsyncState.success([V value]) : this(AsyncStatus.SUCCESS, value);

  static AsyncState<V> failed<V>(dynamic error, [V value]) {
    if (error is AsyncError)
      return AsyncState(AsyncStatus.FAILED, value, error);

    print(error);
    if (error is Error) print(error.stackTrace);
    return AsyncState(
        AsyncStatus.FAILED, value, AsyncError.appError(cause: error));
  }

  bool get hasValue => value != null;
  bool get hasNoValue => value == null;
  bool get isValueEmpty =>
      value is List && (value == null || (value as List).isEmpty);
  bool get isValueNotEmpty =>
      value is List && value != null && (value as List).isNotEmpty;

  bool get isSuccessful => status == AsyncStatus.SUCCESS;
  bool get isNotSuccessful => status != AsyncStatus.SUCCESS;
  bool get isIdle => status == AsyncStatus.IDLE;
  bool get isUseless =>
      status == AsyncStatus.IDLE || status == AsyncStatus.FAILED;
  bool get isFailed => status == AsyncStatus.FAILED;
  bool get isComplete =>
      status == AsyncStatus.FAILED || status == AsyncStatus.SUCCESS;
  bool get isInProgress => status == AsyncStatus.IN_PROGRESS;
  bool get isNotInProgress => status != AsyncStatus.IN_PROGRESS;

  AsyncState<V> copyWith({V value}) =>
      new AsyncState<V>(this.status, value ?? this.value, this.error);

  operator ==(o) {
    return o is AsyncState &&
        o != null &&
        status == o.status &&
        value == o.value;
  }

  @override
  int get hashCode => 0;

  @override
  String toString() {
    return ' ${super.runtimeType}: $status';
  }
}

enum AsyncErrorSeverity { DANGER, WARNING }

enum AsyncErrorType {
  NETWORK_PROBLEMS,
  SERVER_ERROR,
  APP_ERROR,
  API_ERROR,
  FORBIDDEN,
}
enum ApiErrorCode {
  WITH_MESSAGE,
}

class AsyncError {
  final AsyncErrorType type;
  final AsyncErrorSeverity severity;
  final dynamic cause;

  const AsyncError(this.type, this.severity, {this.cause});

  const AsyncError.error(AsyncErrorType kind)
      : this(kind, AsyncErrorSeverity.DANGER);
  const AsyncError.networkProblems({dynamic cause})
      : this(AsyncErrorType.NETWORK_PROBLEMS, AsyncErrorSeverity.WARNING,
            cause: cause);
  const AsyncError.serverError({dynamic cause})
      : this(AsyncErrorType.SERVER_ERROR, AsyncErrorSeverity.DANGER,
            cause: cause);
  const AsyncError.appError({dynamic cause})
      : this(AsyncErrorType.APP_ERROR, AsyncErrorSeverity.DANGER, cause: cause);
  static ApiError<T> apiError<T>(ApiErrorCode type, [T data]) {
    return ApiError(type, data);
  }

  @override
  String toString() => 'AsyncError{type: $type, severity: $severity';
}

class ApiError<T> extends AsyncError {
  final ApiErrorCode code;
  final T data;

  ApiError(this.code, this.data)
      : super(AsyncErrorType.API_ERROR, AsyncErrorSeverity.WARNING);

  String toString() => 'ApiError{code: $code}';
}
