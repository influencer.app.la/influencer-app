/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:app/actions/domain/brand.actions.dart';
import 'package:app/models/auth.models.dart';
import 'package:app/models/domain.models.dart' as models;
import 'package:app/utils/app.dart';
import 'package:app/utils/async_state.dart';
import 'package:app/views/components/notifications.dart';
import 'package:app/views/screens/main/brand/influencer.screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';

import 'package:app/models/domain.models.dart';
import 'package:app/store/app.store.dart';
import 'package:app/views/components/bars.dart';
import 'package:app/views/components/form.dart';
import 'package:app/views/theme.dart';

class ManageGigScreen extends StatefulWidget {
  final Gig gig;

  ManageGigScreen(this.gig);

  @override
  _ManageGigScreenState createState() => _ManageGigScreenState();
}

class _ManageGigScreenState extends State<ManageGigScreen> {
  final _scrollController = ScrollController();
  Gig _gig;
  bool _showTopbarDimmer = false;

  @override
  void initState() {
    _gig = widget.gig;
    super.initState();
  }

  _modelInit(model) {
    setState(() => _showTopbarDimmer = true);
  }

  _modelDidChange(_ViewModel model) {
    if (model.hasLoadMyGigsStateChanged && model.loadMyGigsState.isSuccessful) {
      setState(() {
        _gig = model.loadMyGigsState.value
            .firstWhere((g) => g.id == _gig.id, orElse: () => _gig);
      });
    }
    if (model.hasDeleteGigFailed) {
      Notifications.showAsyncError(context, model.deleteGigState.error);
    }
    if (model.hasDeleteGigSucceed) {
      Navigator.pop(context);
      Notifications.showSuccess(context, message: 'Gig has been deleted');
    }
    if (model.hasStartGigFailed) {
      Notifications.showAsyncError(context, model.startGigState.error);
    }
    if (model.hasStartGigSucceed) {
      Notifications.showSuccess(context, message: 'Gig started successfully');
    }
    if (model.hasCloseGigFailed) {
      Notifications.showAsyncError(context, model.closeGigState.error);
    }
    if (model.hasCloseGigSucceed) {
      Notifications.showSuccess(context, message: 'Gig closed successfully');
    }
    if (model.hasCancelGigFailed) {
      Notifications.showAsyncError(context, model.cancelGigState.error);
    }
    if (model.hasCancelGigSucceed) {
      Notifications.showSuccess(context, message: 'Gig cancelled successfully');
    }
  }

  _delete(_ViewModel model, bool confirm) {
    if (confirm) {
      return showDialog(
          context: context,
          builder: (context) => AlertDialog(
                contentPadding: AppDimensions.dialogContentPadding,
                shape: AppDecorations.dialogShape,
                title:
                    Text('Delete this Gig?', style: AppStyles.dialogTitleText),
                actions: <Widget>[
                  FlatButton(
                    child: Text('NO', style: AppStyles.dialogMinorActionText),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  FlatButton(
                    child:
                        Text('DELETE', style: AppStyles.dialogDangerActionText),
                    onPressed: () {
                      Navigator.of(context).pop();
                      _delete(model, false);
                    },
                  ),
                ],
              ));
    }
    model.deleteGig(_gig.id);
  }

  _start(_ViewModel model, bool confirm) {
    if (confirm) {
      return showDialog(
          context: context,
          builder: (context) => AlertDialog(
                shape: AppDecorations.dialogShape,
                title:
                    Text('Start this Gig?', style: AppStyles.dialogTitleText),
                content: Text(
                    'You have selected ${_gig.hires.length} ' +
                        (_gig.hires.length == 1 ? 'applicant' : 'applicants') +
                        ' to hire for this gig',
                    style: AppStyles.dialogContentText),
                contentPadding: AppDimensions.dialogContentPadding,
                actions: <Widget>[
                  FlatButton(
                    child: Text('NO', style: AppStyles.dialogMinorActionText),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  FlatButton(
                    child: Text('START GIG',
                        style: AppStyles.dialogMajorActionText
                            .copyWith(color: AppColors.success)),
                    onPressed: () {
                      Navigator.of(context).pop();
                      _start(model, false);
                    },
                  ),
                ],
              ));
    }
    model.startGig(_gig);
  }

  _cancel(_ViewModel model, bool confirm) {
    if (confirm) {
      return showDialog(
          context: context,
          builder: (context) => AlertDialog(
                shape: AppDecorations.dialogShape,
                title:
                    Text('Cancel this Gig?', style: AppStyles.dialogTitleText),
                content: Text(
                    'You haven\'t selected an applicant to hire for this gig which is why it will be cancelled',
                    style: AppStyles.dialogContentText),
                contentPadding: AppDimensions.dialogContentPadding,
                actions: <Widget>[
                  FlatButton(
                    child: Text('NO', style: AppStyles.dialogMinorActionText),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  FlatButton(
                    child: Text('CANCEL GIG',
                        style: AppStyles.dialogDangerActionText),
                    onPressed: () {
                      Navigator.of(context).pop();
                      _cancel(model, false);
                    },
                  ),
                ],
              ));
    }
    model.cangelGig(_gig);
  }

  _close(_ViewModel model, bool confirm) {
    if (confirm) {
      return showDialog(
          context: context,
          builder: (context) => AlertDialog(
                shape: AppDecorations.dialogShape,
                title:
                    Text('Close this Gig?', style: AppStyles.dialogTitleText),
                content: _gig.feedbacks.length == _gig.hires.length
                    ? null
                    : Text(
                        'You haven\'t left feedback for ' +
                            (_gig.hires
                                .where((h) =>
                                    _gig.feedbacks.indexWhere(
                                        (f) => f.receiver.id == h.id) ==
                                    -1)
                                .map((h) => h.fullname)
                                .join(', ')) +
                            '. Are you sure you want to close this gig?',
                        style: AppStyles.dialogContentText),
                contentPadding: AppDimensions.dialogContentPadding,
                actions: <Widget>[
                  FlatButton(
                    child: Text('NO', style: AppStyles.dialogMinorActionText),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  FlatButton(
                    child: Text('CLOSE GIG',
                        style: AppStyles.dialogMajorActionText),
                    onPressed: () {
                      Navigator.of(context).pop();
                      _close(model, false);
                    },
                  ),
                ],
              ));
    }
    model.closeGig(_gig);
  }

  void _feedbackDialog(models.Profile worker, models.Feedback feedback) {
    models.Feedback newFeedback;
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              shape: AppDecorations.dialogShape,
              title: Text('Feedback for ${worker.fullname}',
                  style: AppStyles.dialogTitleText),
              content: _FeedbackDialogContent(_gig, worker, feedback,
                  (changedFeedback) => newFeedback = changedFeedback),
              contentPadding: AppDimensions.dialogContentPadding,
              actions: <Widget>[
                FlatButton(
                  child: Text('CANCEL', style: AppStyles.dialogMinorActionText),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child: Text('OK', style: AppStyles.dialogMajorActionText),
                  onPressed: () {
                    setState(() {
                      _gig = _gig.copyWith(
                        feedbacks: feedback == null
                            ? (List.from(_gig.feedbacks)..add(newFeedback))
                            : _gig.feedbacks
                                .map((item) =>
                                    item == feedback ? newFeedback : feedback)
                                .toList(),
                      );
                    });
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ));
  }

  _scrollToBottom() {
    _scrollController.animateTo(
      _scrollController.position.maxScrollExtent,
      curve: Curves.easeIn,
      duration: AppDurations.standartAnimation,
    );
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
        converter: (store) => _ViewModel(store),
        onInitialBuild: _modelInit,
        onDidChange: _modelDidChange,
        builder: (context, model) {
          final profile = model.loginState.value?.profile;
          if (profile == null) return SizedBox();
          return Scaffold(
            backgroundColor: Colors.white,
            body: CustomScrollView(
              physics: ClampingScrollPhysics(),
              controller: _scrollController,
              slivers: <Widget>[
                _topbar(model),
                SliverList(
                    delegate: SliverChildListDelegate(<Widget>[
                  _body(model, profile),
                ]))
              ],
            ),
          );
        });
  }

  Widget _body(_ViewModel model, Profile profile) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 25),
          Row(
            children: <Widget>[
              SizedBox(width: 2),
              Icon(
                FontAwesomeIcons.mapMarkerAlt,
                color: AppColors.secondary,
                size: 20,
              ),
              SizedBox(width: 20),
              Text(_gig.location.name,
                  style: AppStyles.primaryText.copyWith(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  )),
            ],
          ),
          SizedBox(height: 10),
          Padding(
            padding: EdgeInsets.only(left: 45),
            child: Text(
              _gig.location.address,
              style: AppStyles.secondaryText,
            ),
          ),
          SizedBox(height: 20),
          Row(
            children: <Widget>[
              SizedBox(width: 2),
              Icon(
                FontAwesomeIcons.calendarAlt,
                color: AppColors.secondary,
                size: 20,
              ),
              SizedBox(width: 20),
              Padding(
                padding: const EdgeInsets.only(top: 4),
                child: Text(DateFormat.yMd().format(_gig.datetime),
                    style: AppStyles.primaryText.copyWith(fontSize: 18)),
              ),
            ],
          ),
          SizedBox(height: 20),
          Row(
            children: <Widget>[
              SizedBox(width: 3),
              Icon(
                FontAwesomeIcons.infoCircle,
                color: AppColors.secondary,
                size: 20,
              ),
              SizedBox(width: 20),
              Text(enumToString(_gig.status, capitalCase: true),
                  style: AppStyles.primaryText.copyWith(fontSize: 18)),
            ],
          ),
          if (_gig.status == GigStatus.OPEN ||
              _gig.status == GigStatus.CANCELLED)
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Row(
                children: <Widget>[
                  Icon(
                    FontAwesomeIcons.users,
                    color: AppColors.secondary,
                    size: 20,
                  ),
                  SizedBox(width: 20),
                  Padding(
                    padding: const EdgeInsets.only(top: 4),
                    child: Text(
                        '${_gig.applicants.length} ${_gig.applicants.length == 1 ? "applicant" : "applicants"}',
                        style: AppStyles.primaryText.copyWith(fontSize: 18)),
                  ),
                ],
              ),
            ),
          if (_gig.status == GigStatus.OPEN ||
              _gig.status == GigStatus.CANCELLED)
            for (final applicant in _gig.applicants)
              _applicantListItem(
                  applicant,
                  _gig.hires.indexWhere((h) => h.id == applicant.id) > -1,
                  _gig.status == GigStatus.OPEN &&
                      _gig.hires.indexWhere((h) => h.id == applicant.id) == -1,
                  false,
                  false),
          if (_gig.status != GigStatus.CANCELLED)
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Row(
                children: <Widget>[
                  Icon(
                    FontAwesomeIcons.userFriends,
                    color: AppColors.secondary,
                    size: 20,
                  ),
                  SizedBox(width: 20),
                  Padding(
                    padding: const EdgeInsets.only(top: 4),
                    child: Text(
                        '${_gig.hires.length} ${_gig.hires.length == 1 ? "hire" : "hires"}',
                        style: AppStyles.primaryText.copyWith(fontSize: 18)),
                  ),
                ],
              ),
            ),
          if (_gig.status != GigStatus.CANCELLED)
            for (final applicant in _gig.hires)
              _applicantListItem(
                applicant,
                true,
                _gig.status == GigStatus.OPEN,
                _gig.status == GigStatus.STARTED ||
                    _gig.status == GigStatus.CLOSED,
                _gig.status == GigStatus.STARTED,
              ),
          SizedBox(height: 30),
          if (_gig.status == GigStatus.OPEN)
            if (_gig.hires.isNotEmpty)
              FormButton(
                text: 'HIRE & START GIG',
                color: AppColors.success,
                onPressed: () => _start(model, true),
                progress: model.startGigState.isInProgress,
                disabled: model.startGigState.isInProgress,
              )
            else
              FormButton(
                text: 'CANCEL GIG',
                color: AppColors.danger,
                onPressed: () => _cancel(model, true),
                progress: model.cancelGigState.isInProgress,
                disabled: model.cancelGigState.isInProgress,
              )
          else if (_gig.status == GigStatus.STARTED)
            FormButton(
              text: 'CLOSE GIG',
              color: AppColors.primary,
              onPressed: () => _close(model, true),
              progress: model.closeGigState.isInProgress,
              disabled: model.closeGigState.isInProgress,
            ),
          SizedBox(height: 20),
        ],
      ),
    );
  }

  Widget _applicantListItem(Profile applicant, bool hired, bool showHireAction,
      bool showFeedback, bool allowChangeFeedback) {
    return Container(
      width: double.infinity,
      decoration: AppDecorations.underline(),
      margin: EdgeInsets.only(left: 45, right: 5),
      padding: EdgeInsets.only(top: 5, bottom: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkResponse(
            onTap: () {
              Navigator.push(
                  context,
                  AppRouteTransitions.local(
                      (_) => InfluencerScreen(profile: applicant)));
            },
            child: Padding(
              padding: EdgeInsets.fromLTRB(0, 8, 10, 8),
              child: Text(applicant.fullname,
                  style: AppStyles.secondaryText.copyWith(
                      color: AppColors.hyperlink, fontWeight: FontWeight.bold)),
            ),
          ),
          if (showHireAction)
            IconButton(
              icon: Icon(
                  hired
                      ? FontAwesomeIcons.userMinus
                      : FontAwesomeIcons.userCheck,
                  size: 16,
                  color: hired ? AppColors.danger : AppColors.success),
              onPressed: () {
                setState(() {
                  _gig = _gig.copyWith(
                      hires: hired
                          ? _gig.hires
                              .where((h) => h.id != applicant.id)
                              .toList()
                          : (List.from(_gig.hires)..add(applicant)));
                  if (!hired)
                    Future.delayed(AppDurations.waitSetState, _scrollToBottom);
                });
              },
            ),
          if (showFeedback) _feedbackWidget(applicant, allowChangeFeedback),
        ],
      ),
    );
  }

  Widget _feedbackWidget(Profile worker, bool allowChangeFeedback) {
    final feedback = _gig.feedbacks.firstWhere(
        (item) => item.receiver.id == worker.id,
        orElse: () => null);
    if (feedback == null && !allowChangeFeedback)
      return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Text('No feedback given',
            style: AppStyles.secondaryText
                .copyWith(color: Colors.grey, fontStyle: FontStyle.italic)),
      );

    final List<Widget> stars = [];
    for (var i = 1; i <= 5; i++) {
      if (feedback != null && feedback.rating >= i)
        stars.add(Icon(
          FontAwesomeIcons.solidStar,
          size: 18,
          color: AppColors.action,
        ));
      else
        stars.add(Icon(
          FontAwesomeIcons.star,
          size: 18,
          color: AppColors.inactive,
        ));
    }
    return Flexible(
      child: InkResponse(
        onTap: !allowChangeFeedback
            ? null
            : () {
                _feedbackDialog(worker, feedback);
              },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: stars
                  .map((item) => Padding(
                        padding: EdgeInsets.only(left: 3, top: 8),
                        child: item,
                      ))
                  .toList(),
            ),
            if (feedback != null && feedback.text.isNotEmpty)
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: Text(
                  feedback.text,
                  textAlign: TextAlign.right,
                  style: AppStyles.secondaryText.copyWith(
                      color: Colors.grey, fontStyle: FontStyle.italic),
                ),
              ),
          ],
        ),
      ),
    );
  }

  Widget _topbar(_ViewModel model) {
    final mediaQuery = MediaQuery.of(context);
    final width = mediaQuery.size.width;
    final imageHeight = (width * 3 / 4).floorToDouble();
    return SliverAppBar(
      pinned: true,
      expandedHeight: imageHeight,
      brightness: Brightness.dark,
      backgroundColor: AppColors.primary,
      title: Padding(
        padding: AppDimensions.topBarContentPadding,
        child: Text(widget.gig.name,
            style: AppStyles.topBarTitle.copyWith(color: Colors.white)),
      ),
      centerTitle: true,
      leading: TopBarAction.back(
        () => Navigator.pop(context),
        color: Colors.white,
      ),
      actions: <Widget>[
        if (_gig.status == GigStatus.OPEN)
          TopBarAction.delete(
            () => _delete(model, true),
            progress: model.deleteGigState.isInProgress,
          ),
      ],
      flexibleSpace: FlexibleSpaceBar(
        background: Stack(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              height: double.infinity,
              child: Hero(
                tag: widget.gig.id,
                child: Image.network(
                  widget.gig.photo.uri,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            AnimatedOpacity(
              duration: Duration(milliseconds: 300),
              opacity: _showTopbarDimmer ? 1 : 0,
              child: Container(
                width: double.infinity,
                height: imageHeight,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.black.withOpacity(.83),
                    Colors.transparent,
                  ],
                )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _FeedbackDialogContent extends StatefulWidget {
  final models.Gig gig;
  final models.Profile worker;
  final models.Feedback currentFeedback;
  final Function(models.Feedback) onChanged;

  const _FeedbackDialogContent(
      this.gig, this.worker, this.currentFeedback, this.onChanged);

  @override
  __FeedbackDialogContentState createState() => __FeedbackDialogContentState();
}

class __FeedbackDialogContentState extends State<_FeedbackDialogContent> {
  final _commentController = TextEditingController();
  models.Feedback _feedback;

  @override
  void initState() {
    _feedback = widget.currentFeedback == null
        ? new models.Feedback(
            '', 5, widget.gig.owner, widget.worker, widget.gig)
        : widget.currentFeedback.copyWith();
    if (widget.currentFeedback == null) widget.onChanged(_feedback);
    _commentController.text = _feedback.text;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> stars = [];
    for (var i = 1; i <= 5; i++) {
      final star = (_feedback.rating >= i)
          ? Icon(
              FontAwesomeIcons.solidStar,
              size: 18,
              color: AppColors.action,
            )
          : Icon(
              FontAwesomeIcons.star,
              size: 18,
              color: AppColors.inactive,
            );
      stars.add(InkResponse(
        onTap: () {
          final newFeedback = _feedback.copyWith(rating: i);
          setState(() => _feedback = newFeedback);
          widget.onChanged(newFeedback);
        },
        child: Padding(
          padding: EdgeInsets.only(left: 6),
          child: star,
        ),
      ));
    }
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 10),
          decoration: AppDecorations.formFieldBox,
          child: TextField(
            controller: _commentController,
            maxLines: 3,
            inputFormatters: [LengthLimitingTextInputFormatter(500)],
            autofocus: true,
            style: AppStyles.dialogInputText,
            textCapitalization: TextCapitalization.sentences,
            decoration: InputDecoration(
              hintText: 'Comment',
              hintStyle: AppStyles.dialogInputHint,
              border: InputBorder.none,
              contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 5),
            ),
            onChanged: (text) {
              final newFeedback = _feedback.copyWith(text: text);
              setState(() => _feedback = newFeedback);
              widget.onChanged(newFeedback);
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(3, 12, 3, 0),
          child: Row(
            children: [
              Expanded(child: Text('Rating:', style: AppStyles.secondaryText)),
              ...stars,
            ],
          ),
        ),
      ],
    );
  }
}

class _ViewModel {
  final AppState _state;
  final Function _dispatch;

  _ViewModel(Store<AppState> store)
      : _state = store.state,
        _dispatch = store.dispatch;

  AsyncState<Session> get loginState => _state.authState.loginState;
  AsyncState<List<Gig>> get loadMyGigsState =>
      _state.domainState.loadMyGigsState;
  AsyncState<String> get deleteGigState => _state.domainState.deleteGig;
  AsyncState<Gig> get startGigState => _state.domainState.startGig;
  AsyncState<Gig> get closeGigState => _state.domainState.closeGig;
  AsyncState<Gig> get cancelGigState => _state.domainState.cancelGig;

  void deleteGig(String id) => _dispatch(new DeleteGig(id));
  void startGig(Gig gig) => _dispatch(new StartGig(gig));
  void closeGig(Gig gig) => _dispatch(new CloseGig(gig));
  void cangelGig(Gig gig) => _dispatch(new CancelGig(gig));

  bool get hasStartGigSucceed {
    final prev = _state.prevState?.domainState?.startGig;
    return prev != _state.domainState.startGig &&
        _state.domainState.startGig.isSuccessful;
  }

  bool get hasStartGigFailed {
    final prev = _state.prevState?.domainState?.startGig;
    return prev != _state.domainState.startGig &&
        _state.domainState.startGig.isFailed;
  }

  bool get hasCloseGigSucceed {
    final prev = _state.prevState?.domainState?.closeGig;
    return prev != _state.domainState.closeGig &&
        _state.domainState.closeGig.isSuccessful;
  }

  bool get hasCloseGigFailed {
    final prev = _state.prevState?.domainState?.closeGig;
    return prev != _state.domainState.closeGig &&
        _state.domainState.closeGig.isFailed;
  }

  bool get hasCancelGigSucceed {
    final prev = _state.prevState?.domainState?.cancelGig;
    return prev != _state.domainState.cancelGig &&
        _state.domainState.cancelGig.isSuccessful;
  }

  bool get hasCancelGigFailed {
    final prev = _state.prevState?.domainState?.cancelGig;
    return prev != _state.domainState.cancelGig &&
        _state.domainState.cancelGig.isFailed;
  }

  bool get hasDeleteGigFailed {
    final prev = _state.prevState?.domainState?.deleteGig;
    return prev != _state.domainState.deleteGig &&
        _state.domainState.deleteGig.isFailed;
  }

  bool get hasDeleteGigSucceed {
    final prev = _state.prevState?.domainState?.deleteGig;
    return prev != _state.domainState.deleteGig &&
        _state.domainState.deleteGig.isSuccessful;
  }

  bool get hasLoadMyGigsStateChanged {
    final prev = _state.prevState?.domainState?.loadMyGigsState;
    return prev != _state.domainState.loadMyGigsState;
  }

  operator ==(o) {
    return o is _ViewModel &&
        loadMyGigsState == o.loadMyGigsState &&
        closeGigState == o.closeGigState &&
        startGigState == o.startGigState &&
        cancelGigState == o.cancelGigState &&
        deleteGigState == o.deleteGigState;
  }

  @override
  int get hashCode => 0;
}
