/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance software development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */
import 'package:redux_thunk/redux_thunk.dart';

import 'package:redux/redux.dart';

import '../utils/async_state.dart';
import '../store/app.store.dart';
import 'api.dart';

abstract class AsyncAction {
  ThunkAction<AppState> execute(ApiClient api);
}

abstract class AsyncStateChanged<V> {
  AsyncState<V> get state;
}

Middleware<AppState> asyncActionMiddleware(ApiClient api) =>
    (store, action, next) {
      if (action is AsyncAction) {
        store.dispatch(action.execute(api));
      } else {
        next(action);
      }
    };
