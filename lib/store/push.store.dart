/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:app/utils/app.dart';
import 'package:app/utils/async_state.dart';

class ResetNewPushMessageState {}

class ChangeNewPushState {
  final PushMessage message;

  ChangeNewPushState(this.message);

  @override
  String toString() => '$runtimeType: $message';
}

class ChangeUpdatePushTokenState {
  final AsyncState<String> state;

  ChangeUpdatePushTokenState(this.state);

  @override
  String toString() => '$runtimeType.${enumToString(state.status)}';
}

class PushState {
  final AsyncState<String> updatePushToken;
  final PushMessage newMessage;
  final List<PushMessage> history;

  PushState(this.updatePushToken, this.newMessage, this.history);

  PushState.initial() : this(AsyncState.empty(), null, []);

  PushState copyWith({
    AsyncState<String> updatePushToken,
    PushMessage newMessage,
    List<PushMessage> history,
  }) =>
      PushState(
        updatePushToken ?? this.updatePushToken,
        newMessage ?? this.newMessage,
        history ?? this.history,
      );
}

PushState pushReducer(final PushState state, dynamic action) {
  if (action is ChangeUpdatePushTokenState) {
    return state.copyWith(
      updatePushToken: action.state,
    );
  }
  if (action is ResetNewPushMessageState) {
    return new PushState(state.updatePushToken, null, state.history);
  }
  if (action is ChangeNewPushState) {
    return state.copyWith(
      newMessage: action.message,
      history: List<PushMessage>.from(state.history)..add(action.message),
    );
  }
  return state;
}
