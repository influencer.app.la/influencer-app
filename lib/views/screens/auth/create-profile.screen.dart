/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */
import 'dart:async';

import 'package:app/actions/auth/login.action.dart';
import 'package:app/c.dart';
import 'package:app/models/auth.models.dart';
import 'package:app/utils/app.dart';
import 'package:app/utils/async_state.dart';
import 'package:app/utils/views.dart';
import 'package:app/views/components/notifications.dart';
import 'package:app/views/screens/main/main.screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:redux/redux.dart';

import 'package:app/models/domain.models.dart';
import 'package:app/views/components/background.dart';
import 'package:app/views/components/bars.dart';
import 'package:app/views/theme.dart';

import 'package:app/store/app.store.dart';
import 'package:url_launcher/url_launcher.dart';
import 'auth.screen.dart';

class CreateProfileScreen extends StatefulWidget {
  @override
  _CreateProfileScreenState createState() => _CreateProfileScreenState();
}

enum _Mode { SELECT_TYPE, CONNECT_SM_PROFILE, WAIT_IG_PROFILE }
enum _SocialMedia { INSTAGRAM }

class _CreateProfileScreenState extends State<CreateProfileScreen> {
  Timer _checkProfileCreatedTimer;
  // state
  var _mode = _Mode.SELECT_TYPE;
  _SocialMedia _connectSocialMedia;
  ProfileType _profileType;

  void dispose() {
    _cancelCheckProfileCreatedTimerSafe();
    super.dispose();
  }

  _modelInit(_ViewModel model) {}

  _modelDidChange(_ViewModel model) {
    if (model.loginState.isNotSuccessful)
      Navigator.pushReplacement(
          context, AppRouteTransitions.none((_) => AuthScreen()));
    if (model.hasCheckProfileCreatedFailed) {
      setState(() {
        _cancelCheckProfileCreatedTimerSafe();
      });
    }
    if (model.checkProfileCreatedState.isSuccessful &&
        model.checkProfileCreatedState.value != null) {
      return Navigator.pushReplacement(
          context, AppRouteTransitions.fade((_) => MainScreen()));
    }
  }

  void _startCheckProfileCreatedTimer(_ViewModel model) {
    _checkProfileCreatedTimer = Timer.periodic(Duration(seconds: 3), (_) {
      if (model.checkProfileCreatedState.isNotInProgress)
        model.checkProfileCreated();
    });
    model.checkProfileCreated();
  }

  void _cancelCheckProfileCreatedTimerSafe() {
    if (_checkProfileCreatedTimer != null && _checkProfileCreatedTimer.isActive)
      _checkProfileCreatedTimer.cancel();
  }

  void _cancelConnectSocialMedia() {
    _cancelCheckProfileCreatedTimerSafe();
    setState(() {
      setState(() {
        _connectSocialMedia = null;
        _mode = _Mode.CONNECT_SM_PROFILE;
      });
    });
  }

  void _connectInstagramAccount(_ViewModel model) {
    final redirectUri = '$API_SCHEMA://$API_HOST:$API_PORT/profiles/create_ig';
    final uriState =
        '${model.loginState.value.user.id}+${enumToString(_profileType)}';
    final uri = 'https://api.instagram.com/oauth/authorize/?client_id=' +
        '$INSTAGRAM_CLIENT_ID&response_type=code&state=$uriState&redirect_uri=$redirectUri';
    if (DEV_MODE) print('instagram auth uri: $uri');
    canLaunch(uri).then((can) {
      if (!can) {
        Notifications.showDanger(context,
            message: 'Failed to launch internet browser');
        return;
      }
      launch(uri).then((_) {
        _startCheckProfileCreatedTimer(model);
        setState(() => _mode = _Mode.WAIT_IG_PROFILE);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(store),
      onInitialBuild: _modelInit,
      onDidChange: _modelDidChange,
      builder: (context, model) => Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: _topbar(model),
        body: _body(context, model),
      ),
    );
  }

  Widget _body(BuildContext context, _ViewModel model) {
    return Stack(
      children: <Widget>[
        AuthBackground(),
        Material(
          color: Colors.transparent,
          child: _mode == _Mode.CONNECT_SM_PROFILE
              ? _connectSocialMediaProfile(context, model)
              : _mode == _Mode.WAIT_IG_PROFILE
                  ? _waitIgProfile(context, model)
                  : _selectType(context, model),
        ),
      ],
    );
  }

  Widget _waitIgProfile(BuildContext context, _ViewModel model) {
    return Center(
      child: pickAsyncStateWidget<Profile>(
        model.checkProfileCreatedState,
        onFail: (error) => BackPrint.fromAsyncError(error,
            message: 'Failed to connect Instagram account',
            onTryAgain: () =>
                setState(() => _startCheckProfileCreatedTimer(model))),
        orElse: () => BackPrint(
          icon: FontAwesomeIcons.instagram,
          title: 'Instagram',
          message: 'Connecting your account...',
          buttonTitle: 'Cancel',
          onButtonPress: _cancelConnectSocialMedia,
        ),
      ),
    );
  }

  Widget _connectSocialMediaProfile(BuildContext context, _ViewModel model) {
    final color = _connectSocialMedia == _SocialMedia.INSTAGRAM
        ? Color(0xffD14E6A)
        : Colors.grey;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: InkWell(
            borderRadius: BorderRadius.circular(9.0),
            child: Container(
                margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                padding: EdgeInsets.symmetric(vertical: 10),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(9.0),
                    border: Border.all(
                      color: color,
                      width: 1.0,
                    )),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 8.0, vertical: 3),
                      child: Icon(
                        FontAwesomeIcons.instagram,
                        color: color,
                        size: 40,
                      ),
                    ),
                    Text(
                      'Connect Instagram',
                      style: AppStyles.header.copyWith(color: color),
                    ),
                  ],
                )),
            onTap: () =>
                setState(() => _connectSocialMedia = _SocialMedia.INSTAGRAM),
          ),
        ),
      ],
    );
  }

  _selectType(BuildContext context, _ViewModel model) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        _selectTypeItem(
          'influencer',
          'gigs, accessories, creators',
          ProfileType.INFLUENCER,
        ),
        _selectTypeItem(
          'brand',
          'influencer profiles or create campaign',
          ProfileType.BRAND,
        ),
      ],
    );
  }

  Widget _selectTypeItem(String title, String desc, ProfileType type) {
    final color = _profileType == type ? AppColors.primary : Colors.grey;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: InkWell(
        borderRadius: BorderRadius.circular(9.0),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          padding: EdgeInsets.symmetric(vertical: 10),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(9.0),
              border: Border.all(
                color: color,
                width: 1.0,
              )),
          child: Column(
            children: <Widget>[
              Text(
                title,
                style: AppStyles.primaryText.copyWith(
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                  color: color,
                ),
              ),
              SizedBox(height: 5),
              Text(
                desc,
                style: AppStyles.secondaryText.copyWith(
                  color: color,
                ),
              ),
            ],
          ),
        ),
        onTap: () => setState(() => _profileType = type),
      ),
    );
  }

  TopBar _topbar(_ViewModel model) {
    return TopBar(
        title: _mode == _Mode.WAIT_IG_PROFILE
            ? '...in Instagram'
            : _mode == _Mode.CONNECT_SM_PROFILE
                ? '...in social media'
                : 'You are an...',
        line: false,
        progress: _checkProfileCreatedTimer?.isActive == true,
        leftAction: _mode == _Mode.CONNECT_SM_PROFILE
            ? TopBarAction.back(() => setState(() => _mode = _Mode.SELECT_TYPE))
            : _mode == _Mode.WAIT_IG_PROFILE
                ? TopBarAction.cancel(_cancelConnectSocialMedia)
                : null,
        rightAction: _mode == _Mode.SELECT_TYPE && _profileType != null
            ? TopBarAction.next(
                () => setState(() => _mode = _Mode.CONNECT_SM_PROFILE))
            : _mode == _Mode.CONNECT_SM_PROFILE && _connectSocialMedia != null
                ? TopBarAction.next(() => _connectInstagramAccount(model))
                : null);
  }
}

class _ViewModel {
  final AppState _state;
  final Function _dispatch;

  _ViewModel(Store<AppState> store)
      : _state = store.state,
        _dispatch = store.dispatch;

  AsyncState<Session> get loginState => _state.authState.loginState;
  AsyncState<Profile> get checkProfileCreatedState =>
      _state.authState.checkProfileCreated;

  void checkProfileCreated() => _dispatch(CheckProfileCreated());

  bool get hasCheckProfileCreatedFailed {
    final prev = _state.prevState?.authState?.checkProfileCreated;
    return prev != _state.authState.checkProfileCreated &&
        _state.authState.checkProfileCreated.isFailed;
  }

  operator ==(o) {
    return o is _ViewModel &&
        this.checkProfileCreatedState == o.checkProfileCreatedState &&
        this.loginState == o.loginState;
  }

  @override
  int get hashCode => 0;
}
