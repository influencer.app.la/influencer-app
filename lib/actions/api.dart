/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */
import 'dart:convert';
import 'dart:io';
import 'package:path/path.dart';
import 'package:async/async.dart';
import 'package:app/models/auth.models.dart';
import 'package:http/http.dart' as Http;

import '../utils/async_state.dart';
import '../c.dart';

enum HttpMethod { GET, POST, PUT, DELETE }

class ApiClient {
  final _client = Http.Client();

  dynamic _pickRespose(Http.BaseResponse response,
      {Function onBadRequest, onFobbiden, onSuccess, orElse}) async {
    switch (response.statusCode) {
      case 400:
        return await onBadRequest();
      case 401:
      case 403:
        return await onFobbiden();
      case 200:
      case 201:
      case 202:
        return await onSuccess();
      default:
        return await orElse();
    }
  }

  void _throwApiError(Map<String, dynamic> body) {
    ApiErrorCode code;
    if (body['message'] != null) {
      code = ApiErrorCode.WITH_MESSAGE;
    } else {
      final _code = body.remove('code') as String;
      code = ApiErrorCode.values.firstWhere(
          (t) => t.toString() == '${t.runtimeType}.$_code',
          orElse: () => null);
    }
    throw code == null
        ? AsyncError.serverError()
        : AsyncError.apiError(code, body);
  }

  Uri _uri(String host, String path, Map<String, String> params) {
    return Uri.http('${host ?? API_HOST}:$API_PORT', path, params);
  }

  Map<String, String> _headers(Session session, {Map<String, dynamic> body}) {
    final headers = new Map<String, String>();
    if (session != null) headers['Authorization'] = 'Bearer ${session.token}';
    if (body != null) headers['Content-Type'] = 'application/json';
    return headers;
  }

  Future<dynamic> call(
    String path, {
    String host,
    HttpMethod method = HttpMethod.GET,
    Map<String, dynamic> body,
    Map<String, String> params,
    Session session,
  }) async {
    try {
      final uri = _uri(host, path, params);
      final headers = _headers(session, body: body);
      final bodyJson = body != null ? jsonEncode(body) : null;
      final future = method == HttpMethod.GET
          ? _client.get(uri, headers: headers)
          : method == HttpMethod.POST
              ? _client.post(uri, headers: headers, body: bodyJson)
              : method == HttpMethod.PUT
                  ? _client.put(uri, headers: headers, body: bodyJson)
                  : _client.delete(uri, headers: headers);
      final response = await future;
      return _pickRespose(
        response,
        onBadRequest: () {
          final body = jsonDecode(response.body);
          _throwApiError(body);
        },
        onFobbiden: () {
          if (session != null) session.onExpired();
          throw AsyncError.error(AsyncErrorType.FORBIDDEN);
        },
        onSuccess: () {
          return jsonDecode(response.body);
        },
        orElse: () {
          throw AsyncError.serverError(
              cause: {'statusCode': response.statusCode});
        },
      );
    } catch (err) {
      if (err is IOException || err is Http.ClientException)
        throw AsyncError.networkProblems(cause: err);
      throw err;
    }
  }

  Future<dynamic> upload(
    String path,
    File file,
    String fieldName, {
    String host,
    Map<String, String> params,
    Session session,
  }) async {
    final uri = _uri(host, path, params);
    var request = new Http.MultipartRequest("POST", uri);
    request.headers.addAll(_headers(session));
    final stream = new Http.ByteStream(DelegatingStream.typed(file.openRead()));
    final length = await file.length();
    request.files.add(new Http.MultipartFile(fieldName, stream, length,
        filename: basename(file.path)));
    final response = await request.send();
    return _pickRespose(
      response,
      onBadRequest: () async {
        final json = await response.stream.transform(utf8.decoder).first;
        final body = jsonDecode(json);
        _throwApiError(body);
      },
      onFobbiden: () {
        if (session != null) session.onExpired();
        throw AsyncError.error(AsyncErrorType.FORBIDDEN);
      },
      onSuccess: () async {
        final json = await response.stream.transform(utf8.decoder).first;
        return jsonDecode(json);
      },
      orElse: () {
        throw AsyncError.serverError();
      },
    );
  }
}
