/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */
import 'package:app/actions/domain/lookup-location.action.dart';
import 'package:app/utils/views.dart';
import 'package:app/views/components/background.dart';
import 'package:app/views/components/bars.dart';
import 'package:app/views/components/notifications.dart';
import 'package:app/views/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'package:app/models/domain.models.dart';
import 'package:app/store/app.store.dart';
import 'package:app/utils/async_state.dart';
import 'package:rxdart/rxdart.dart';

class LookupLocationScreen extends StatefulWidget {
  final Function(Location location) onPicked;
  final bool requireLocationName;

  const LookupLocationScreen({
    Key key,
    @required this.onPicked,
    this.requireLocationName = false,
  }) : super(key: key);

  @override
  _LookupLocationScreenState createState() => _LookupLocationScreenState();
}

class _LookupLocationScreenState extends State<LookupLocationScreen> {
  final _queryController = TextEditingController();
  final _locationNameController = TextEditingController();
  final _searchSubject = BehaviorSubject<String>();

  _ViewModel _model;

  @override
  void initState() {
    _searchSubject
        .debounceTime(Duration(seconds: 1))
        .where((query) =>
            _model != null &&
            query.isNotEmpty &&
            _model.lookupLocationState.isNotInProgress)
        .listen((query) => _model.lookupLocation(query));
    super.initState();
  }

  @override
  void dispose() {
    _searchSubject.close();
    super.dispose();
  }

  _modelInit(_ViewModel model) {
    _model = model;
    if (model.lookupLocationState.isSuccessful)
      setState(
          () => _queryController.text = model.lookupLocationState.value.query);
  }

  _modelDidChange(_ViewModel model) {
    _model = model;

    if (model.hasLookupLocationFailed) {
      Notifications.showAsyncError(context, model.lookupLocationState.error);
    }
  }

  void _search() {
    _searchSubject.add('');
    _model.lookupLocation(_queryController.value.text);
  }

  void _enterName(Location location, Function(Location) onEntered) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              shape: AppDecorations.dialogShape,
              title: Text(location.address, style: AppStyles.dialogTitleText),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Location Name:',
                      style: AppStyles.dialogContentText
                          .copyWith(fontWeight: FontWeight.w600)),
                  SizedBox(height: 5),
                  Container(
                    decoration: AppDecorations.dialogTextField,
                    child: TextField(
                      controller: _locationNameController,
                      maxLines: 1,
                      inputFormatters: [LengthLimitingTextInputFormatter(100)],
                      autofocus: true,
                      style: AppStyles.formFieldText,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.fromLTRB(8, 6, 8, 5),
                      ),
                    ),
                  ),
                ],
              ),
              contentPadding: EdgeInsets.fromLTRB(25, 20, 25, 0),
              actions: <Widget>[
                FlatButton(
                  child: Text('Cancel', style: AppStyles.dialogMinorActionText),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child: Text('OK', style: AppStyles.dialogMajorActionText),
                  onPressed: () {
                    Navigator.of(context).pop();
                    onEntered(
                        location.copyWith(name: _locationNameController.text));
                  },
                ),
              ],
            ));
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(store),
      onInitialBuild: _modelInit,
      onDidChange: _modelDidChange,
      builder: (context, model) {
        return Scaffold(
          appBar: _topbar(model),
          body: _body(model),
        );
      },
    );
  }

  Widget _listItem(_ViewModel model, Location location) {
    return InkWell(
      onTap: () {
        if (location.name == null && widget.requireLocationName == true) {
          _locationNameController.text = _queryController.text;
          _locationNameController.selection = TextSelection(
              baseOffset: 0, extentOffset: _queryController.text.length);
          return _enterName(location, (l) {
            widget.onPicked(l);
            Navigator.pop(context);
          });
        }
        widget.onPicked(location);
        Navigator.pop(context);
      },
      child: Container(
        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
        constraints: BoxConstraints(minHeight: 70),
        decoration: AppDecorations.underline(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            if (location.name != null)
              Padding(
                  padding: EdgeInsets.only(bottom: 3),
                  child: Text(location.name,
                      style: AppStyles.header.copyWith(fontSize: 18))),
            Text(location.address, style: AppStyles.secondaryText),
          ],
        ),
      ),
    );
  }

  Widget _body(_ViewModel model) {
    return pickAsyncStateWidget<LookupLocationValue>(model.lookupLocationState,
        onFail: (error) => BackPrint.fromAsyncError(error, onTryAgain: _search),
        onValue: (data) {
          if (data.locations.isEmpty)
            return BackPrint.empty(message: 'Location not found');
          return ListView.builder(
            itemCount: data.locations.length,
            itemBuilder: (_, index) => _listItem(model, data.locations[index]),
          );
        });
  }

  TopBar _topbar(_ViewModel model) {
    return TopBar(
      titleWidget: Container(
        margin: EdgeInsets.only(top: 5),
        decoration: AppDecorations.formFieldBox,
        child: TextField(
          controller: _queryController,
          maxLines: 1,
          inputFormatters: [LengthLimitingTextInputFormatter(100)],
          autofocus: true,
          textInputAction: TextInputAction.search,
          style: AppStyles.formFieldText,
          decoration: InputDecoration(
            border: InputBorder.none,
            contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 5),
          ),
          onChanged: (query) {
            _searchSubject.add(query);
          },
          onSubmitted: (_) => _search(),
        ),
      ),
      leftAction: TopBarAction.cancel(() => Navigator.pop(context)),
      rightAction: TopBarAction.search(
        _search,
        progress: model.lookupLocationState.isInProgress,
      ),
    );
  }
}

class _ViewModel {
  final AppState _state;
  final Function _dispatch;

  _ViewModel(Store<AppState> store)
      : _state = store.state,
        _dispatch = store.dispatch;

  AsyncState<LookupLocationValue> get lookupLocationState =>
      _state.domainState.lookupLocationState;

  void lookupLocation(String query) => _dispatch(new LookupLocation(query));

  bool get hasLookupLocationFailed {
    final prev = _state.prevState?.domainState?.lookupLocationState;
    return prev != _state.domainState.lookupLocationState &&
        _state.domainState.lookupLocationState.isFailed;
  }

  operator ==(o) {
    return o is _ViewModel && this.lookupLocationState == o.lookupLocationState;
  }

  @override
  int get hashCode => 0;
}
