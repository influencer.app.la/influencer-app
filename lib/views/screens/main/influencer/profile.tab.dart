/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */
import 'package:app/models/auth.models.dart';
import 'package:app/utils/async_state.dart';
import 'package:app/views/components/domain/instagram-profile.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:app/store/app.store.dart';
import 'package:app/views/components/bars.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:app/views/components/background.dart';

class ProfileTab extends BottomBarTab {
  @override
  Widget topBar(BuildContext context, TopBar base) {
    return base;
  }

  @override
  Widget icon() => Icon(FontAwesomeIcons.user);

  @override
  Widget text() => Text('Profile');

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(store),
      builder: (_, model) => _body(context, model),
    );
  }

  Widget _body(BuildContext context, _ViewModel model) {
    final profile = model.loginState.value?.profile?.ig;
    if (profile == null) return BackPrint.danger(message: 'No Profile');
    return InstagramProfileDetails(profile: profile);
  }
}

class _ViewModel {
  final AppState _state;
  final Function _dispatch;

  _ViewModel(Store<AppState> store)
      : _state = store.state,
        _dispatch = store.dispatch;

  AsyncState<Session> get loginState => _state.authState.loginState;

  operator ==(o) {
    return o is _ViewModel;
  }

  @override
  int get hashCode => 0;
}
