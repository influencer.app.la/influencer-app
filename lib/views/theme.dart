/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance software development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:flutter/material.dart';

class AppColors {
  static const primary = Color(0xFF3C599C);
  static const secondary = Color(0xFF028F72);

  static const success = Color(0xff479F4B);
  static const danger = Color(0xFFE56754);
  static const warning = Colors.orange;

  static const action = Colors.orange;
  static const hyperlink = Color(0xFF2979FF);

  static const separator = Color(0xFFe2e2e2);
  static const inactive = Color(0xFFd6d6d6);

  // output/input
  static const darkText = Color(0xff707070);
  static const darkerText = Color(0xff7F7F7F);
  static const darkestText = Color(0xff555555);

  static const inputText = AppColors.darkerText;
  static const inputTextHint = Color(0x99757575);
  static const inputTextDisabled = Color(0x99414141);

  // backgrounds
  static const primaryBg = Color(0xffffffff);
  static const secondaryBg = Color(0xffF1F3F7);
  static const lightBg = primaryBg;
  static const panelBg = secondaryBg;
  static const darkBg = Color(0xffF1F3F7);

  static const backActivityIndicator = action;
  static final backPrintIcon = Colors.grey;
  static final backPrintText = backPrintIcon;

  // borders
  static const secondaryBorder = Color(0xffEAEAEA);

  // dialogs
  static const dialogTitle = darkerText;
  static const dialogMinorAction = dialogTitle;

  // notifications
  static final notificationBg = Color(0xFF3f5ea7);
  static const notificationText = topBarTitle;

  // top bar
  static const topBarBg = primary;
  static const topBarTitle = Colors.white;
  static const topBarIcon = Colors.white;
  static const topBarIconDisabled = Colors.grey;
  static const topBarSearchBarIcon = Color(0xFFD3D3D3);
  static const topBarSearchBarBorder = Color(0xFFE2E1DF);
  static const topBarLine = action;

  // bottom bar
  static const bottomBarBg = topBarBg;
  static const bottomBarItem = Colors.white;
  static final bottomBarItemInactive = Color(0xff888888);
  static const bottomBarItemBudge = Color(0xFFF34246);

  // auth
  static const authSlashBg = panelBg;

  // form
  static const formLabel = darkerText;
  static const formFieldBoxBorder = Color(0x70B0ADA7);
  static const formImagePickerBg = Color(0x70e4e4e4);
  static const formButton = primary;
  static const formButtonDisabled = Colors.grey;
  static const formButtonText = Colors.white;
  static const formHyperlink = Colors.white;
}

class AppStyles {
  static const primaryText = TextStyle(
    fontFamily: 'Nunito',
    fontSize: 16.0,
    color: AppColors.darkText,
  );
  static final primaryTextBig = primaryText.copyWith(fontSize: 20);
  static const secondaryText = TextStyle(
    fontFamily: 'Lato',
    fontSize: 16.0,
    color: AppColors.darkText,
  );

  static final header = primaryText.copyWith(
    fontSize: 20,
    fontWeight: FontWeight.bold,
  );

  static const actionText = TextStyle(
    fontFamily: 'Nunito',
    fontSize: 16.0,
    color: AppColors.action,
    fontWeight: FontWeight.w500,
  );
  static const actionTextMinor = TextStyle(
    fontFamily: 'Nunito',
    fontSize: 14.0,
    color: Colors.grey,
    fontWeight: FontWeight.w500,
  );
  static const notificationTitle = TextStyle(
    fontFamily: 'Nunito',
    fontSize: 18.0,
    color: AppColors.notificationText,
    fontWeight: FontWeight.bold,
  );
  static const notificationMessage = TextStyle(
    fontFamily: 'Nunito',
    fontSize: 16.0,
    color: AppColors.notificationText,
  );

  /*
   * Dialogs
   */
  static const dialogTitleText = TextStyle(
    fontFamily: 'Nunito',
    fontSize: 20.0,
    color: AppColors.dialogTitle,
    fontWeight: FontWeight.w600,
  );
  static const dialogContentText = TextStyle(
    fontFamily: 'Nunito',
    fontSize: 17.0,
    color: AppColors.darkText,
  );
  static const dialogInputText = TextStyle(
    fontFamily: 'Lato',
    fontSize: 17.0,
    color: AppColors.darkText,
  );
  static final dialogInputHint = dialogInputText.copyWith(
    color: AppColors.inputTextHint,
  );
  static const dialogMinorActionText = TextStyle(
    fontFamily: 'Nunito',
    fontSize: 17.0,
    color: AppColors.dialogMinorAction,
  );
  static final dialogMajorActionText = dialogMinorActionText.copyWith(
    color: AppColors.action,
    fontWeight: FontWeight.bold,
  );
  static final dialogDangerActionText = dialogMajorActionText.copyWith(
    color: AppColors.danger,
    fontWeight: FontWeight.bold,
  );

  /*
   *   TopBar
   */
  static final topBarTitle = TextStyle(
    fontFamily: 'Nunito',
    fontSize: 20.0,
    color: AppColors.topBarTitle,
    fontWeight: FontWeight.normal,
  );

  /*
   * Bottom Bar
  */
  static final bottomBarItemTitle = TextStyle(
    fontFamily: 'Nunito',
    fontSize: 17.0,
    fontWeight: FontWeight.normal,
    color: AppColors.bottomBarItemInactive,
  );

  /*
   * Background
  */
  static final backPrintTitle = TextStyle(
    fontFamily: 'Nunito',
    fontSize: 22.0,
    color: AppColors.backPrintText,
    fontWeight: FontWeight.bold,
  );
  static final backPrintMessage =
      backPrintTitle.copyWith(fontSize: 17.0, fontWeight: FontWeight.normal);
  static final backPrintTryAgainButton = actionText;

  /*
   * Form
  */
  static const formFieldText = TextStyle(
    fontFamily: 'Nunito',
    fontSize: 18.0,
    color: AppColors.inputText,
  );
  static final formFieldTextDisabled = formFieldText.copyWith(
    color: AppColors.inputTextDisabled,
  );
  static const formFieldLabel = TextStyle(
    fontSize: 14.0,
    fontFamily: 'Nunito',
    color: AppColors.formLabel,
    fontWeight: FontWeight.w600,
  );
  static const formButtonText = TextStyle(
    fontFamily: 'Nunito',
    fontSize: 18.0,
    fontWeight: FontWeight.w600,
    color: AppColors.formButtonText,
  );
  static final formHyperlink = secondaryText.copyWith(
    color: AppColors.primary,
  );
}

class AppDimensions {
  // notifications
  static const notificationIconSize = 30.0;
  static const notificationMargin = EdgeInsets.fromLTRB(0, 0, 0, 0);
  static const notificationPadding = const EdgeInsets.all(16);
  static const notificationIconPadding = EdgeInsets.only(left: 13);
  static const notificationContentPadding = EdgeInsets.only(left: 10, top: 8);

  // top/bottom bar
  static const topBarHeight = 58.0;
  static const topBarElevation = 1.0;
  static const topBarLineHeight = 2.0;
  static const bottomBarIconPadding = EdgeInsets.fromLTRB(10, 3, 10, 5);

  // form
  static const formTextFieldContentPadding = EdgeInsets.all(10);
  static const formItemHorizontalMargin = 30.0;
  static const formPadding = EdgeInsets.symmetric(horizontal: 10.0);
  static const formLabelPadding = const EdgeInsets.fromLTRB(3, 0, 0, 3);
  static const formTopFieldMargin = EdgeInsets.fromLTRB(
      formItemHorizontalMargin, 5, formItemHorizontalMargin, 0);
  static const formMiddleFieldMargin = EdgeInsets.fromLTRB(
      formItemHorizontalMargin, 10, formItemHorizontalMargin, 0);
  static const formPickerFieldHeight = 46.0;
  static const formImagePickerFieldHeight = 150.0;

  static const formButtonRadius = 30.0;
  static const formButtonContentPadding = EdgeInsets.only(top: 18, bottom: 16);
  static const formTopButtonMargin = EdgeInsets.fromLTRB(
      formItemHorizontalMargin, 30, formItemHorizontalMargin, 0);
  static const formMiddleButtonMargin = formTopFieldMargin;
  static const formBottomButtonMargin = EdgeInsets.fromLTRB(
      formItemHorizontalMargin, 5, formItemHorizontalMargin, 60);
  static const formBottomHyperlinkMargin =
      EdgeInsets.only(top: 13.0, bottom: 15.0);

  static const topBarContentPadding = EdgeInsets.only(top: 5.0);
  static const topBarActionPadding = EdgeInsets.fromLTRB(12.0, 4.0, 12.0, 0.0);

  static const backPrintButtonRadius = 50.0;
  static const backPrintButtonPadding =
      EdgeInsets.symmetric(horizontal: 10.0, vertical: 4.0);

  static const dialogContentPadding = EdgeInsets.fromLTRB(24, 10, 24, 5);
}

class AppDecorations {
  static final card = BoxDecoration(
    color: AppColors.primaryBg,
    borderRadius: BorderRadius.circular(8.0),
    border: Border.all(
      color: AppColors.separator,
      width: 1.0,
    ),
    boxShadow: [
      BoxShadow(
        color: Colors.black.withAlpha(30),
        offset: Offset(2.0, 2.0),
        blurRadius: 2.0,
      )
    ],
  );

  static final bottomBar = BoxDecoration(
    boxShadow: [
      BoxShadow(
        color: Colors.black.withAlpha(30),
        offset: Offset(.0, .0),
        blurRadius: 2.0,
      )
    ],
    // boxShadow: AppShadows.formItem,
  );
  static final borderedButton = BoxDecoration(
    borderRadius: BorderRadius.circular(4.0),
    border: Border.all(
      color: Colors.grey,
      width: 1.0,
    ),
  );
  static final notification = BoxDecoration(
    color: AppColors.notificationBg,
    borderRadius: BorderRadius.all(Radius.circular(15.0)),
    boxShadow: [],
  );

  static final form = BoxDecoration(
    color: AppColors.lightBg,
    borderRadius: BorderRadius.all(Radius.circular(30.0)),
    boxShadow: [
      BoxShadow(
        color: Colors.black.withAlpha(60),
        offset: Offset(1.0, 3.0),
        blurRadius: 6.0,
      )
    ],
  );

  static final formFieldBox = BoxDecoration(
    border: Border.all(
      color: AppColors.formFieldBoxBorder,
      width: 1.0,
    ),
    borderRadius: BorderRadius.circular(10),
    color: Colors.white,
    // boxShadow: AppShadows.formItem,
  );
  static final formImageFieldBox = BoxDecoration(
    border: Border.all(
      color: AppColors.formFieldBoxBorder,
      width: 1.0,
    ),
    borderRadius: BorderRadius.circular(10),
    color: AppColors.formImagePickerBg,
  );

  static final formButtonBox = BoxDecoration(
      // boxShadow: AppShadows.formItem,
      );

  static final backPrintButton = BoxDecoration(
      borderRadius: BorderRadius.circular(AppDimensions.backPrintButtonRadius),
      border: Border.all(color: AppColors.action));

  static final underlineSeparator = BoxDecoration(
    border: Border(bottom: BorderSide(color: AppColors.separator)),
  );

  static underline({Color color = AppColors.separator}) => BoxDecoration(
        border: Border(bottom: BorderSide(color: color)),
      );

  static final dialogShape =
      RoundedRectangleBorder(borderRadius: BorderRadius.circular(8));
  static final dialogTextField = BoxDecoration(
    border: Border.all(
      color: AppColors.formFieldBoxBorder,
      width: 1.5,
    ),
    borderRadius: BorderRadius.circular(6),
  );
}

class AppShadows {
  static final List<BoxShadow> basic = [
    BoxShadow(
      color: Colors.black.withAlpha(60),
      offset: Offset(1.0, 1.0),
      blurRadius: 6.0,
    )
  ];
  static final List<BoxShadow> minY = [
    BoxShadow(
      color: Colors.black.withAlpha(40),
      offset: Offset(.0, 1.0),
      blurRadius: 1.0,
    )
  ];
  static final List<BoxShadow> formItem = [
    BoxShadow(
      color: Colors.black.withAlpha(30),
      offset: Offset(.5, 1.0),
      blurRadius: 4.0,
    )
  ];
  static final notificationBox = [
    BoxShadow(
        color: Colors.black.withAlpha(40),
        offset: Offset(0, 2),
        blurRadius: 0,
        spreadRadius: 0)
  ];
}

class AppRouteTransitions {
  static local(WidgetBuilder builder) {
    return MaterialPageRoute(builder: builder);
  }

  static none(WidgetBuilder builder) {
    return PageRouteBuilder(
        pageBuilder: (ctx, _, __) => builder(ctx),
        transitionsBuilder: (_, ___, __, Widget child) {
          return child;
        });
  }

  static fade(WidgetBuilder builder, {int duration = 200}) {
    return PageRouteBuilder(
        pageBuilder: (ctx, _, __) => builder(ctx),
        transitionDuration: Duration(milliseconds: duration),
        transitionsBuilder: (_, animation, __, Widget child) {
          return new FadeTransition(opacity: animation, child: child);
        });
  }

  static verticalSlide(WidgetBuilder builder, {int duration = 200}) {
    return PageRouteBuilder(
      pageBuilder: (ctx, _, __) => builder(ctx),
      transitionDuration: Duration(milliseconds: duration),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;
        var tween =
            Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }
}

class AppDurations {
  static const standartAnimation = Duration(milliseconds: 250);
  static const waitSetState = Duration(milliseconds: 100);
}

ThemeData appTheme(BuildContext context) {
  final theme = Theme.of(context);
  return ThemeData(
    primaryColor: AppColors.primary,
    primaryColorBrightness: Brightness.dark,
    canvasColor: AppColors.primaryBg,
    accentColor: Colors.grey,

    // not working for snackbar (flat button defaults)
    textTheme: theme.textTheme.copyWith(
        button: AppStyles.actionText,
        // bottom bar item title
        caption: AppStyles.bottomBarItemTitle.copyWith(
          color: AppColors.bottomBarItemInactive,
        )),
  );
}
