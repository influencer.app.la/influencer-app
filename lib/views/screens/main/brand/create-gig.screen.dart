/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'dart:io';

import 'package:app/actions/domain/brand.actions.dart';
import 'package:app/i18n/i18n.dart';
import 'package:app/models/auth.models.dart';
import 'package:app/models/domain.models.dart';
import 'package:app/utils/async_state.dart';
import 'package:app/views/components/bars.dart';
import 'package:app/views/components/form.dart';
import 'package:app/views/components/notifications.dart';
import 'package:app/views/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:image_picker/image_picker.dart';
import 'package:redux/redux.dart';
import 'package:flutter/material.dart';
import 'package:app/store/app.store.dart';

import '../lookup-location.screen.dart';

class CreateGigScreen extends StatefulWidget {
  @override
  _CreateGigScreenState createState() => _CreateGigScreenState();
}

class _CreateGigScreenState extends State<CreateGigScreen> {
  // state
  Gig _gig;
  File _image;

  @override
  void initState() {
    _gig = Gig(name: '', datetime: DateTime.now().add(Duration(days: 1)));
    super.initState();
  }

  _modelInit(_ViewModel model) {
    setState(() {
      _gig = _gig.copyWith(owner: model.loginState.value?.profile);
    });
  }

  _modelDidChange(_ViewModel model) {
    if (model.hasCreateGigFailed) {
      Notifications.showAsyncError(context, model.createGigState.error);
    }
    if (model.hasCreateGigSucceed) {
      Navigator.pop(context);
      Notifications.showSuccess(context, message: 'Gig has been created');
    }
  }

  _create(_ViewModel model) {
    if (_image == null) {
      return Notifications.showDanger(context, message: 'Pick a photo');
    }
    if (_gig.name.isEmpty) {
      return Notifications.showDanger(context, message: 'Enter name');
    }
    if (_gig.location == null) {
      return Notifications.showDanger(context, message: 'Pick the location');
    }
    model.createGig(_gig, _image);
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(store),
      onInitialBuild: _modelInit,
      onDidChange: _modelDidChange,
      builder: (context, model) {
        return Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: _topbar(model),
          body: _body(context, model),
        );
      },
    );
  }

  Widget _body(BuildContext context, _ViewModel model) {
    return SimpleForm(
      keyboardSpaceRatio: 1.1,
      children: <Widget>[
        SizedBox(height: 20),
        FormImagePickerField(
          label: 'Photo',
          value: _image,
          margin: AppDimensions.formTopFieldMargin.copyWith(bottom: 10),
          onTap: () {
            ImagePicker.pickImage(source: ImageSource.gallery)
                .then((image) => setState(() => _image = image));
          },
        ),
        FormTextField(
          label: 'Gig Name',
          maxLength: 20,
          textCapitalization: TextCapitalization.sentences,
          disabled: model.createGigState.isInProgress,
          onChanged: (name) {
            setState(() {
              _gig = _gig.copyWith(name: name);
            });
          },
        ),
        FormPickerField(
          label: 'Date',
          value: _gig.datetime == null ? '' : I18N.formatGigDate(_gig.datetime),
          disabled: model.createGigState.isInProgress,
          onTap: () {
            showModalBottomSheet(
                context: context,
                builder: (context) => SizedBox(
                      height: 220,
                      child: CupertinoDatePicker(
                        mode: CupertinoDatePickerMode.date,
                        initialDateTime: _gig.datetime,
                        onDateTimeChanged: (date) {
                          setState(() {
                            _gig = _gig.copyWith(datetime: date);
                          });
                        },
                      ),
                    ));
          },
        ),
        FormPickerField(
          label: 'Location',
          value: _gig.location?.name ?? '',
          disabled: model.createGigState.isInProgress,
          onTap: () {
            Navigator.push(
                context,
                AppRouteTransitions.verticalSlide((_) => LookupLocationScreen(
                      onPicked: (location) => setState(
                          () => _gig = _gig.copyWith(location: location)),
                      requireLocationName: true,
                    )));
          },
        ),
      ],
    );
  }

  TopBar _topbar(_ViewModel model) {
    return TopBar(
      title: 'New Gig',
      progress: model.createGigState.isInProgress,
      leftAction: TopBarAction.back(() => Navigator.pop(context)),
      rightAction: TopBarAction.save(
        () => _create(model),
        progress: model.createGigState.isInProgress,
      ),
    );
  }
}

class _ViewModel {
  final AppState _state;
  final Function _dispatch;

  _ViewModel(Store<AppState> store)
      : _state = store.state,
        _dispatch = store.dispatch;

  AsyncState<Session> get loginState => _state.authState.loginState;
  AsyncState<Gig> get createGigState => _state.domainState.createGigState;
  void createGig(Gig gig, File image) => _dispatch(new CreateGig(gig, image));

  bool get hasCreateGigFailed {
    final prev = _state.prevState?.domainState?.createGigState;
    return prev != _state.domainState.createGigState &&
        _state.domainState.createGigState.isFailed;
  }

  bool get hasCreateGigSucceed {
    final prev = _state.prevState?.domainState?.createGigState;
    return prev != _state.domainState.createGigState &&
        _state.domainState.createGigState.isSuccessful;
  }

  operator ==(o) {
    return o is _ViewModel && loginState == o.loginState;
  }

  @override
  int get hashCode => 0;
}
