/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */
import 'package:app/actions/api.dart';
import 'package:app/store/app.store.dart';
import 'package:app/store/push.store.dart';
import 'package:app/utils/async_state.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:app/utils/app.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';

import '../actions.dart';

class UpdatePushToken implements AsyncAction {
  @override
  ThunkAction<AppState> execute(api) => (store) async {
        print('UpdatePushToken.execute()');

        final session = store.state.authState.loginState.value;
        if (session == null) return;

        try {
          store.dispatch(
              new ChangeUpdatePushTokenState(AsyncState.inProgress()));

          final _firebaseMessaging = FirebaseMessaging();
          if (defaultTargetPlatform == TargetPlatform.iOS)
            _firebaseMessaging.requestNotificationPermissions(
                IosNotificationSettings(sound: true, alert: true, badge: true));

          _firebaseMessaging.configure(onMessage: (message) async {
            final push = PushMessage(message);
            if (push.code == null) {
              print('recived invalid push (no code), ignoring');
              return null;
            }
            store.dispatch(ChangeNewPushState(push));
          });

          final pushToken = await _firebaseMessaging.getToken();
          final body = {
            'deviceId': await getDeviceId(),
            'pushToken': pushToken,
          };
          await api.call('/devices',
              method: HttpMethod.POST, body: body, session: session);
          store.dispatch(
              new ChangeUpdatePushTokenState(AsyncState.success(pushToken)));
        } catch (e) {
          print('failed to update push token: $e');
          store.dispatch(new ChangeUpdatePushTokenState(AsyncState.failed(e)));
        }
      };
}
