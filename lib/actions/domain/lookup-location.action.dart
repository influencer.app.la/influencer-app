/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:redux_thunk/redux_thunk.dart';

import 'package:app/models/domain.models.dart';
import 'package:app/store/app.store.dart';
import 'package:app/utils/async_state.dart';

import '../actions.dart';

class LookupLocationValue {
  final String query;
  final List<Location> locations;

  LookupLocationValue(this.query, this.locations);
}

class LookupLocationStateChanged {
  final AsyncState<LookupLocationValue> state;

  LookupLocationStateChanged(this.state);

  @override
  String toString() =>
      '$runtimeType.' +
      '${state.status}'.substring('.${state.status.runtimeType}'.length);
}

class LookupLocation implements AsyncAction {
  final String query;

  LookupLocation(this.query);

  @override
  ThunkAction<AppState> execute(api) => (store) async {
        print('LookupLocation.execute()');

        final query = this.query.trim();
        if (query.isEmpty) {
          store.dispatch(LookupLocationStateChanged(
              AsyncState.success(LookupLocationValue('', []))));
          return;
        }

        final session = store.state.authState.loginState.value;
        if (session == null) return;

        store.dispatch(LookupLocationStateChanged(AsyncState.inProgress()));
        try {
          final params = {'_q': query};
          final list =
              await api.call('/locations', params: params, session: session);
          final results =
              (list as List).map((map) => Location.fromJson(map)).toList();
          store.dispatch(LookupLocationStateChanged(
              AsyncState.success(LookupLocationValue(query, results))));
        } catch (error) {
          store.dispatch(LookupLocationStateChanged(AsyncState.failed(error)));
        }
      };
}
