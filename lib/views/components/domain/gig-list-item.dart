/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:app/i18n/i18n.dart';
import 'package:app/models/domain.models.dart';
import 'package:app/views/theme.dart';
import 'package:flutter/material.dart';


class GigListItem extends StatelessWidget {
  final Gig gig;
  final bool applied;

  const GigListItem(this.gig, {this.applied});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          height: 120,
          padding: const EdgeInsets.all(10),
          decoration: AppDecorations.underline(),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Hero(
                tag: gig.id,
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      image: DecorationImage(
                        image: NetworkImage(gig.photo.uri),
                        fit: BoxFit.cover,
                      )),
                ),
              ),
              SizedBox(width: 15),
              Flexible(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      gig.name,
                      style: AppStyles.header.copyWith(fontSize: 18),
                      overflow: TextOverflow.ellipsis,
                    ),
                    Text(
                      gig.location.name,
                      style: AppStyles.primaryText,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      softWrap: true,
                    ),
                    Text(
                      I18N.formatGigDate(gig.datetime),
                      style: AppStyles.secondaryText,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        if (applied == true)
          Positioned(
            top: 0,
            right: 0,
            child: CustomPaint(
              painter: AppliedTringlePainer(),
              child: SizedBox(width: 15, height: 15),
            ),
          ),
      ],
    );
    ;
  }
}

class AppliedTringlePainer extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint();
    paint.color = AppColors.action;
    Path path = new Path();
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height);
    path.lineTo(0, 0);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
