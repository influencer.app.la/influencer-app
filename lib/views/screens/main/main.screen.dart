/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */
import 'package:app/actions/push/update-token.dart';
import 'package:app/store/push.store.dart';
import 'package:app/utils/app.dart';
import 'package:app/views/components/notifications.dart';
import 'package:app/views/screens/auth/auth.screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'package:app/models/auth.models.dart';
import 'package:app/models/domain.models.dart';
import 'package:app/store/app.store.dart';
import 'package:app/utils/async_state.dart';

import '../../theme.dart';
import 'brand/brand.main-screen.dart';
import 'influencer/influencer.main-screen.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    void _modelInit(_ViewModel model) {
      if (model.updatePushTokenState.isNotInProgress) model.updatePushToken();
    }

    void _modelDidChange(_ViewModel model) {
      if (model.loginState.isNotSuccessful) {
        Navigator.popUntil(context, (route) => route.isFirst);
        Navigator.pushReplacement(
            context, AppRouteTransitions.none((_) => AuthScreen()));
        return;
      }
      if (model.hasUpdatePushTokenFailed) {
        Notifications.showAsyncError(context, model.updatePushTokenState.error);
      }
      if (model.newPushMessageState != null) {
        if (model.newPushMessageState.code == 'HIRED') {
          _showHiredPushMessageDialog(context, model.newPushMessageState);
        } else {
          _showPushMessageDialog(context, model.newPushMessageState);
        }
        model.resetNewPushMessageState();
      }
    }

    return StoreConnector<AppState, _ViewModel>(
      converter: (Store store) => _ViewModel(store),
      onInitialBuild: _modelInit,
      onDidChange: _modelDidChange,
      builder: (context, model) {
        if (model.loginState.value?.profile == null) {
          return SizedBox();
        }
        return model.loginState.value.profile.type == ProfileType.INFLUENCER
            ? InfluencerMainScreen()
            : BrandMainScreen();
      },
    );
  }

  void _showHiredPushMessageDialog(BuildContext context, PushMessage push) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              shape: AppDecorations.dialogShape,
              title: Text(push.title, style: AppStyles.dialogTitleText),
              content: Text(push.body, style: AppStyles.dialogContentText),
              contentPadding: AppDimensions.dialogContentPadding,
              actions: <Widget>[
                FlatButton(
                  child: Text('OK', style: AppStyles.dialogMajorActionText),
                  onPressed: () {
                    Navigator.of(context).pop();
                    // todo: open gig screen
                    // Navigator.push(
                    //     context,
                    //     AppRouteTransitions.local(
                    //         (_) => MyGigListScreen(refresh: true)));
                  },
                ),
              ],
            ));
  }

  void _showPushMessageDialog(BuildContext context, PushMessage push) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              shape: AppDecorations.dialogShape,
              title: Text(push.title, style: AppStyles.dialogTitleText),
              content: Text(push.body, style: AppStyles.dialogContentText),
              contentPadding: AppDimensions.dialogContentPadding,
              actions: <Widget>[
                FlatButton(
                  child: Text('OK', style: AppStyles.dialogMajorActionText),
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ],
            ));
  }
}

class _ViewModel {
  final AppState _state;
  final Function _dispatch;

  _ViewModel(Store<AppState> store)
      : _state = store.state,
        _dispatch = store.dispatch;

  AsyncState<Session> get loginState => _state.authState.loginState;
  PushMessage get newPushMessageState => _state.push.newMessage;
  AsyncState<String> get updatePushTokenState => _state.push.updatePushToken;

  void updatePushToken() => _dispatch(new UpdatePushToken());
  void resetNewPushMessageState() => _dispatch(new ResetNewPushMessageState());

  bool get hasUpdatePushTokenFailed {
    final prev = _state.prevState?.push?.updatePushToken;
    return prev != _state.push.updatePushToken &&
        _state.push.updatePushToken.isFailed;
  }

  operator ==(o) {
    return o is _ViewModel &&
        this.loginState == o.loginState &&
        this.updatePushTokenState == o.updatePushTokenState &&
        this.newPushMessageState == o.newPushMessageState;
  }

  @override
  int get hashCode => 0;
}
