/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance software development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */
import 'dart:convert';

import 'package:app/actions/app.actions.dart';
import 'package:app/actions/auth/signup.action.dart';
import 'package:redux/redux.dart';

import 'package:app/actions/auth/login.action.dart';
import 'package:app/models/auth.models.dart';
import 'package:app/models/domain.models.dart';
import 'package:app/store/app.store.dart';
import 'package:app/utils/async_state.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SetCheckProfileCreatedState {
  final AsyncState<Profile> state;

  SetCheckProfileCreatedState(this.state);

  @override
  String toString() =>
      'runtimeType.' +
      '${state.status}'.substring('.${state.status.runtimeType}'.length);
}

class AuthState {
  final AsyncState<Session> loginState;
  final AsyncState<Session> signUpState;
  final AsyncState<Profile> checkProfileCreated;

  AuthState(
    this.loginState,
    this.signUpState,
    this.checkProfileCreated,
  );

  AuthState.initial()
      : this(
          AsyncState.empty(),
          AsyncState.empty(),
          AsyncState.empty(),
        );

  AuthState copyWith({
    AsyncState<Session> loginState,
    AsyncState<Session> signUpState,
    AsyncState<Profile> checkProfileCreated,
  }) =>
      AuthState(
        loginState ?? this.loginState,
        signUpState ?? this.signUpState,
        checkProfileCreated ?? this.checkProfileCreated,
      );
}

AuthState authReducer(AppState state, dynamic action) {
  if (action is LoginStateChanged) {
    return state.authState.copyWith(loginState: action.state);
  }
  if (action is SignUpStateChanged) {
    return state.authState.copyWith(
      signUpState: action.state,
      loginState:
          action.state.isSuccessful ? action.state : state.authState.loginState,
    );
  }
  if (action is SetCheckProfileCreatedState) {
    return state.authState.copyWith(
      checkProfileCreated: action.state,
      loginState: action.state.isSuccessful &&
              action.state.value != null &&
              state.authState.loginState.isSuccessful
          ? AsyncState.success(state.authState.loginState.value
              .copyWith(profile: action.state.value))
          : state.authState.loginState,
    );
  }
  return state.authState;
}

void persistSessionMiddleware(
    Store<AppState> store, dynamic action, NextDispatcher next) {
  if (action is InitAppStateChanged && action.state.isSuccessful) {
    _restoreSession(store).then((session) {
      if (session != null)
        store.dispatch(LoginStateChanged(AsyncState.success(session)));
    });
  }
  if (action is LoginStateChanged || action is SignUpStateChanged) {
    _persistSession(action.state.value).then((_) => next(action));
    return;
  }
  if (action is SetCheckProfileCreatedState && action.state.value != null) {
    _restoreSession(store).then((session) {
      _persistSession(session.copyWith(profile: action.state.value))
          .then((_) => next(action));
    });
    return;
  }
  next(action);
}

const STORAGE_KEY = 'session_v4';

Future<Session> _restoreSession(Store<AppState> store) async {
  final prefs = await SharedPreferences.getInstance();
  final json = prefs.getString(STORAGE_KEY);
  if (json != null)
    return Session.fromMap(
      jsonDecode(json),
      () => store.dispatch(new Logout()),
    );
  return null;
}

Future _persistSession(Session session) async {
  final prefs = await SharedPreferences.getInstance();
  if (session != null)
    prefs.setString(STORAGE_KEY, jsonEncode(session.toMap()));
  else
    prefs.remove(STORAGE_KEY);
}
