/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'dart:io';

import 'package:app/actions/actions.dart';
import 'package:app/actions/api.dart';
import 'package:app/models/domain.models.dart';
import 'package:app/store/app.store.dart';
import 'package:app/store/domain.store.dart';
import 'package:app/utils/async_state.dart';
import 'package:redux_thunk/redux_thunk.dart';

class CreateGig implements AsyncAction {
  final Gig gig;
  final File photo;

  CreateGig(this.gig, this.photo);

  @override
  ThunkAction<AppState> execute(api) => (store) async {
        print('CreateGig.execute()');

        final session = store.state.authState.loginState.value;
        if (session == null) return;

        store.dispatch(ChangeCreateGigState(AsyncState.inProgress()));
        try {
          final List photoList =
              await api.upload('/upload', photo, 'files', session: session);
          if (photoList.isEmpty) throw AsyncError.serverError();
          final body = {
            'name': gig.name,
            'photo': photoList.first['id'],
            'location': gig.location.id != null
                ? gig.location.id
                : {
                    'name': gig.location.name,
                    'address': gig.location.address,
                    'latitude': gig.location.latitude,
                    'longitude': gig.location.longitude,
                  },
            'datetime': gig.datetime.toIso8601String(),
          };
          final map = await api.call('/gigs/self',
              method: HttpMethod.POST, session: session, body: body);
          final entity = Gig.fromJson(map);
          store.dispatch(ChangeCreateGigState(AsyncState.success(entity)));
        } catch (error) {
          store.dispatch(ChangeCreateGigState(AsyncState.failed(error)));
        }
      };
}

class DeleteGig implements AsyncAction {
  final String id;

  DeleteGig(this.id);

  @override
  ThunkAction<AppState> execute(api) => (store) async {
        print('DeleteGig.execute()');

        final session = store.state.authState.loginState.value;
        if (session == null) return;

        store.dispatch(ChangeDeleteGigState(AsyncState.inProgress()));
        try {
          await api.call('/gigs/$id',
              method: HttpMethod.DELETE, session: session);
          store.dispatch(ChangeDeleteGigState(AsyncState.success(id)));
        } catch (error) {
          store.dispatch(ChangeDeleteGigState(AsyncState.failed(error)));
        }
      };
}

class StartGig implements AsyncAction {
  final Gig gig;

  StartGig(this.gig);

  @override
  ThunkAction<AppState> execute(api) => (store) async {
        print('StartGig.execute()');

        final session = store.state.authState.loginState.value;
        if (session == null) return;

        store.dispatch(ChangeStartGigState(AsyncState.inProgress()));
        try {
          final body = {'hires': this.gig.hires.map((h) => h.id).toList()};
          await api.call('/gigs/${this.gig.id}/start',
              method: HttpMethod.PUT, body: body, session: session);
          final gig = this.gig.copyWith(status: GigStatus.STARTED);
          store.dispatch(ChangeStartGigState(AsyncState.success(gig)));
        } catch (error) {
          store.dispatch(ChangeStartGigState(AsyncState.failed(error)));
        }
      };
}

class CloseGig implements AsyncAction {
  final Gig gig;

  CloseGig(this.gig);

  @override
  ThunkAction<AppState> execute(api) => (store) async {
        print('CloseGig.execute()');

        final session = store.state.authState.loginState.value;
        if (session == null) return;

        store.dispatch(ChangeCloseGigState(AsyncState.inProgress()));
        try {
          final body = {
            'feedbacks': gig.feedbacks
                .map((f) => {
                      'text': f.text,
                      'rating': f.rating,
                      'gig': f.gig.id,
                      'writer': f.writer.id,
                      'receiver': f.receiver.id,
                    })
                .toList()
          };
          await api.call('/gigs/${this.gig.id}/close',
              method: HttpMethod.PUT, body: body, session: session);
          store.dispatch(ChangeCloseGigState(
              AsyncState.success(gig.copyWith(status: GigStatus.CLOSED))));
        } catch (error) {
          store.dispatch(ChangeCloseGigState(AsyncState.failed(error)));
        }
      };
}

class CancelGig implements AsyncAction {
  final Gig gig;

  CancelGig(this.gig);

  @override
  ThunkAction<AppState> execute(api) => (store) async {
        print('CancelGig.execute()');

        final session = store.state.authState.loginState.value;
        if (session == null) return;

        store.dispatch(ChangeCancelGigState(AsyncState.inProgress()));
        try {
          await api.call('/gigs/${this.gig.id}/cancel',
              method: HttpMethod.PUT, session: session);
          final gig = this.gig.copyWith(status: GigStatus.CANCELLED);
          store.dispatch(ChangeCancelGigState(AsyncState.success(gig)));
        } catch (error) {
          store.dispatch(ChangeCancelGigState(AsyncState.failed(error)));
        }
      };
}
