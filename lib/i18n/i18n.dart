// ignore_for_file: non_constant_identifier_names
/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance software development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:intl/intl.dart';

import '../utils/async_state.dart';

final gigDateFormat = DateFormat('MMMM dd, yyyy');
final birthdayDateFormat = DateFormat.yMd();

class I18N {
  static const appname = 'Influencer';

  static const ok = 'OK';
  static const cancel = 'Cancel';
  static const tryAgain = 'Try again';
  static const emptyList = 'Empty';

  static const NA = 'N/A';

  static String formatGigDate(DateTime date) => gigDateFormat.format(date);
  static String formatBirthday(DateTime date) => birthdayDateFormat.format(date);

  static String fromAsyncError(AsyncError error) {
    switch (error.type) {
      case AsyncErrorType.APP_ERROR:
        return 'Application Error';
      case AsyncErrorType.SERVER_ERROR:
        return 'Server Error';
      case AsyncErrorType.NETWORK_PROBLEMS:
        return 'Network problems';
      case AsyncErrorType.FORBIDDEN:
        return 'Forbidden';
      case AsyncErrorType.API_ERROR:
        final apiError = error as ApiError;
        switch (apiError.code) {
          case ApiErrorCode.WITH_MESSAGE:
            return apiError.data['message'];
          default:
            return 'Unknown API Error';
        }
        break;
      default:
        return 'Unknown Error';
    }
  }
}
