/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'domain.models.dart';

class Session {
  final User user;
  final Profile profile;
  final String token;
  final Function onExpired;

  Session(this.user, this.profile, this.token, this.onExpired);

  Session.fromMap(Map map, this.onExpired)
      : this.user = User.fromMap(map['user']),
        this.profile =
            map['profile'] == null ? null : Profile.fromJson(map['profile']),
        this.token = map['jwt'];

  Map toMap() => {
        'user': user.toMap(),
        'profile': profile?.toMap(),
        'jwt': token,
      };

  Session copyWith({Profile profile}) =>
      Session(this.user, profile ?? this.profile, this.token, this.onExpired);
}

class User {
  final String id;
  final String username;
  final String email;

  User.fromMap(Map map)
      : this.id = map['_id'],
        this.username = map['username'],
        this.email = map['email'];

  Map toMap() => {
        '_id': this.id,
        'username': this.username,
        'email': this.email,
      };
}
