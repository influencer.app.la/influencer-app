/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance software development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:app/store/push.store.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'package:app/actions/api.dart';
import 'package:app/actions/app.actions.dart';
import 'package:app/store/auth.store.dart';

import '../utils/async_state.dart';
import 'domain.store.dart';
import '../actions/actions.dart';

class AppState {
  final AsyncState<void> initState;
  final AuthState authState;
  final DomainState domainState;
  final PushState push;
  final AppState prevState;

  const AppState(
    this.initState,
    this.authState,
    this.domainState,
    this.push, {
    this.prevState,
  });

  static AppState initial() {
    return AppState(
      AsyncState.empty(),
      AuthState.initial(),
      DomainState.initial(),
      PushState.initial(),
    );
  }

  AppState copyWith({
    AsyncState<void> initState,
    AuthState authState,
    DomainState domainState,
    PushState push,
    String activeHeater,
  }) =>
      AppState(
        initState ?? this.initState,
        authState ?? this.authState,
        domainState ?? this.domainState,
        push ?? this.push,
        prevState: this,
      );

  operator ==(o) {
    return o is AppState &&
        initState == o.initState &&
        authState == o.authState &&
        push == o.push &&
        domainState == o.domainState;
  }

  @override
  int get hashCode => 0;
}

class AppStore extends Store<AppState> {
  AppStore(ApiClient api)
      : super(
          appReducer,
          initialState: AppState.initial(),
          middleware: [
            thunkMiddleware,
            asyncActionMiddleware(api),
            persistSessionMiddleware,
          ],
          distinct: true,
          syncStream: true,
        );
}

AppState appReducer(final AppState state, dynamic action) {
  print('dispatched: $action');

  return state.copyWith(
    initState: action is InitAppStateChanged ? action.state : state.initState,
    authState: authReducer(state, action),
    domainState: domainReducer(state, action),
    push: pushReducer(state.push, action),
  );
}
