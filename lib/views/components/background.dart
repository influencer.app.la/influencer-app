/*
 * Developed in 2018 by Oleg Khalidov (brooth@gmail.com).
 *
 * Freelance Mobile Development:
 * UpWork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../i18n/i18n.dart';
import '../../utils/async_state.dart';
import '../theme.dart';
import 'activity.dart';

class BackActivityIndicator extends StatelessWidget {
  final bool linear;

  BackActivityIndicator({this.linear = false});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width / 4),
        child: linear
            ? LinearActivityIndicator(color: AppColors.backActivityIndicator)
            : CircularActivityIndicator(
                color: AppColors.backActivityIndicator, size: 24.0),
      ),
    );
  }
}

class BackPrint extends StatelessWidget {
  final IconData icon;
  final double iconSize;
  final String title;
  final String message;
  final String buttonTitle;
  final GestureTapCallback onButtonPress;

  BackPrint({
    this.icon,
    this.iconSize = 45,
    this.title,
    this.message,
    this.buttonTitle,
    this.onButtonPress,
  });

  static BackPrint fromAsyncError(AsyncError error,
      {String message, GestureTapCallback onTryAgain}) {
    final icon = error.severity == AsyncErrorSeverity.WARNING
        ? FontAwesomeIcons.exclamationTriangle
        : FontAwesomeIcons.bomb;
    final title = I18N.fromAsyncError(error);
    return BackPrint(
        icon: icon, title: title, message: message, onButtonPress: onTryAgain);
  }

  BackPrint.empty({String message, GestureTapCallback onTryAgain})
      : this(
          icon: Icons.search,
          message: message ?? I18N.emptyList,
          onButtonPress: onTryAgain,
        );

  BackPrint.danger({String title, String message})
      : this(
          icon: FontAwesomeIcons.bomb,
          title: title,
          message: message,
        );

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30.0),
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            if (icon != null)
              Icon(icon, size: iconSize, color: AppColors.backPrintIcon),
            if (title != null)
              Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 3),
                child: Text(
                  title,
                  style: AppStyles.backPrintTitle,
                  textAlign: TextAlign.center,
                ),
              ),
            if (message != null)
              Padding(
                  padding: EdgeInsets.only(top: 5),
                  child: Text(
                    message,
                    style: AppStyles.backPrintMessage,
                    textAlign: TextAlign.center,
                  )),
            if (onButtonPress != null)
              Container(
                margin: EdgeInsets.only(top: 25.0),
                decoration: AppDecorations.backPrintButton,
                child: InkWell(
                    onTap: onButtonPress,
                    borderRadius: AppDecorations.backPrintButton.borderRadius,
                    child: Padding(
                      padding: AppDimensions.backPrintButtonPadding,
                      child: Text(buttonTitle ?? I18N.tryAgain,
                          style: AppStyles.backPrintTryAgainButton),
                    )),
              ),
          ],
        ),
      ),
    );
  }
}

class AuthBackground extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return CustomPaint(
      painter: AuthBackgroundArcsPainter(),
      size: Size(screenSize.width, screenSize.height),
    );
  }
}

class AuthBackgroundArcsPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint();
    paint.color = AppColors.authSlashBg;

    // top arc
    Path path = new Path();
    path.lineTo(0.0, size.height - 90);
    path.lineTo(size.width, size.height - 250);
    path.lineTo(size.width, 0);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
