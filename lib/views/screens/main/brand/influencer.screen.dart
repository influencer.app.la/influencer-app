/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */
import 'package:app/actions/domain/load-profile-feedbacks.action.dart';
import 'package:app/models/domain.models.dart';
import 'package:app/store/domain.store.dart';
import 'package:app/utils/async_state.dart';
import 'package:app/utils/views.dart';
import 'package:app/views/components/background.dart';
import 'package:app/views/components/headers.dart';
import 'package:app/views/theme.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:redux/redux.dart';
import 'package:app/models/domain.models.dart' as models;
import 'package:app/store/app.store.dart';
import 'package:app/views/components/bars.dart';
import 'package:app/views/components/domain/instagram-profile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class InfluencerScreen extends StatefulWidget {
  final Profile profile;

  const InfluencerScreen({Key key, @required this.profile}) : super(key: key);

  @override
  _InfluencerScreenState createState() => _InfluencerScreenState();
}

class _InfluencerScreenState extends State<InfluencerScreen> {
  void _modelInit(_ViewModel model) {
    model.loadProfileFeedbacks(widget.profile);
  }

  void _modelDidChange(_ViewModel model) {}

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
        converter: (store) => _ViewModel(store),
        onInitialBuild: _modelInit,
        onDidChange: _modelDidChange,
        builder: (context, model) {
          return Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: _topbar(context, model),
            body: _body(context, model),
          );
        });
  }

  Widget _body(BuildContext context, _ViewModel model) {
    return ListView(
      children: <Widget>[
        InstagramProfileDetails(
            profile: widget.profile.ig, showUsername: false),
        _feedbacks(context, model),
      ],
    );
  }

  TopBar _topbar(BuildContext context, _ViewModel model) {
    return TopBar(
      title: '@${widget.profile.ig.username}',
      leftAction: TopBarAction.back(() => Navigator.pop(context)),
    );
  }

  Widget _feedbacks(BuildContext context, _ViewModel model) {
    return Column(
      children: <Widget>[
        SizedBox(height: 20),
        SectionHeader('Feedbacks'),
        pickAsyncStateWidget<LoadProfileFeedbacksPayload>(
            model.loadProfileFeedbacksState,
            onFail: (error) => _sectionBackPrint(BackPrint.fromAsyncError(error,
                onTryAgain: () => model.loadProfileFeedbacks(widget.profile))),
            onProgress: () => _sectionBackPrint(BackActivityIndicator()),
            onSuccess: (data) {
              if (data.feedbacks.isEmpty)
                return _sectionBackPrint(BackPrint(
                    icon: FontAwesomeIcons.meh, message: 'No feedbacks given'));
              return SizedBox(
                height: 100,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: data.feedbacks.length,
                    itemBuilder: (_, index) {
                      final feedback = data.feedbacks[index];
                      return _feedbackItem(feedback);
                    }),
              );
            }),
      ],
    );
  }

  Widget _feedbackItem(models.Feedback feedback) {
    final List<Widget> stars = [];
    for (var i = 1; i <= 5; i++) {
      if (feedback != null && feedback.rating >= i)
        stars.add(Icon(
          FontAwesomeIcons.solidStar,
          size: 15,
          color: AppColors.action,
        ));
      else
        stars.add(Icon(
          FontAwesomeIcons.star,
          size: 15,
          color: AppColors.inactive,
        ));
    }
    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: 300),
      child: Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(5),
            height: 90,
            width: 90,
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(feedback.gig.photo.uri),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(5),
                border: Border.all(color: AppColors.secondaryBorder)),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(5, 5, 5, 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: Text(feedback.gig.name,
                        style: AppStyles.header.copyWith(fontSize: 16)),
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        '${feedback.writer.fullname}: ',
                        style: AppStyles.secondaryText.copyWith(fontSize: 15),
                      ),
                    ]..addAll(stars.map((star) => Padding(
                        padding: EdgeInsets.only(left: 2), child: star))),
                  ),
                  SizedBox(height: 2),
                  Text(
                    feedback.text,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: AppStyles.secondaryText.copyWith(
                        color: Colors.grey, fontStyle: FontStyle.italic),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _sectionBackPrint(Widget child) {
    return Container(
      constraints: BoxConstraints(minHeight: 50),
      padding: EdgeInsets.symmetric(vertical: 20),
      child: child,
    );
  }
}

class _ViewModel {
  final AppState _state;
  final Function _dispatch;

  _ViewModel(Store<AppState> store)
      : _state = store.state,
        _dispatch = store.dispatch;

  AsyncState<LoadProfileFeedbacksPayload> get loadProfileFeedbacksState =>
      _state.domainState.loadProfileFeedbacks;

  void loadProfileFeedbacks(Profile profile) =>
      _dispatch(new LoadProfileFeedbacks(profile));
  void resetLoadProfileFeedbacksState() =>
      _dispatch(ChangeLoadProfileFeedbacksState(AsyncState.empty()));

  operator ==(o) {
    return o is _ViewModel &&
        loadProfileFeedbacksState == o.loadProfileFeedbacksState;
  }

  @override
  int get hashCode => 0;
}
