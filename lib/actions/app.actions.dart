/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */
import 'package:redux_thunk/redux_thunk.dart';

import '../c.dart';
import '../store/app.store.dart';
import '../utils/async_state.dart';
import 'actions.dart';

class InitAppStateChanged extends AsyncStateChanged {
  final AsyncState<void> state;

  InitAppStateChanged(this.state);

  @override
  String toString() =>
      '$runtimeType.' +
      '${state.status}'.substring('.${state.status.runtimeType}'.length);
}

class InitApp implements AsyncAction {
  @override
  ThunkAction<AppState> execute(api) => (store) async {
        print('InitAppAction.execute()');

        store.dispatch(InitAppStateChanged(AsyncState.inProgress()));
        try {
          await Future.delayed(Duration(milliseconds: DEV_MODE ? 1000 : 2000));
          store.dispatch(InitAppStateChanged(AsyncState.success()));
        } catch (error) {
          store.dispatch(InitAppStateChanged(AsyncState.failed(error)));
        }
      };
}
