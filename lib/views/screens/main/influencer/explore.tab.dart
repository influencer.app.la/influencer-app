/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */
import 'package:app/i18n/i18n.dart';
import 'package:app/views/components/background.dart';
import 'package:app/views/components/bars.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:app/actions/domain/influencer/load-open-gigs.action.dart';
import 'package:app/models/domain.models.dart';
import 'package:app/store/app.store.dart';
import 'package:app/utils/async_state.dart';
import 'package:app/utils/views.dart';

import '../../../theme.dart';
import 'open-gig-list.screen.dart';

class ExploreTab extends BottomBarTab {
  @override
  Widget topBar(BuildContext context, TopBar base) {
    return base;
  }

  @override
  Widget icon() => Icon(FontAwesomeIcons.compass);

  @override
  Widget text() => Text('Explore');

  void _modelInit(_ViewModel model) {
    if (model.loadOpenGigsState.isUseless) model.loadOpenGigs();
  }

  void _modelChange(_ViewModel model) {}

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(store),
      onInitialBuild: _modelInit,
      onDidChange: _modelChange,
      builder: (context, model) {
        return _body(context, model);
      },
    );
  }

  Widget _body(BuildContext context, _ViewModel model) {
    return RefreshIndicator(
      onRefresh: () => Future.delayed(Duration(milliseconds: 350), () {
        model.loadOpenGigs();
      }),
      child: ListView(
        children: <Widget>[
          Center(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 15, 0, 5),
              child: Text('Gigs', style: AppStyles.header),
            ),
          ),
          _openGigsPager(context, model),
        ],
      ),
    );
  }

  Widget _openGigsPager(BuildContext context, _ViewModel model) {
    return InkWell(
      child: pickAsyncStateWidget<List<Gig>>(
        model.loadOpenGigsState,
        onFail: (error) => _backPrintCard(BackPrint(
            icon: FontAwesomeIcons.exclamationTriangle,
            iconSize: 35,
            message: I18N.fromAsyncError(error))),
        onEmpty: () => _backPrintCard(BackPrint.empty(message: "No gig found")),
        onValue: (data) => Container(
          height: 140,
          padding: const EdgeInsets.all(8.0),
          child: PageView.builder(
            scrollDirection: Axis.horizontal,
            controller: PageController(
              viewportFraction: 0.95,
              initialPage: 0,
            ),
            itemCount: data.length,
            itemBuilder: (context, idx) {
              final gig = data[idx];
              return _gigItem(gig);
            },
          ),
        ),
        orElse: () => SizedBox(height: 140, child: BackActivityIndicator()),
      ),
      onTap: () => Navigator.push(
          context, AppRouteTransitions.local((_) => OpenGigListScreen())),
    );
  }

  Widget _gigItem(Gig gig) {
    return Container(
      margin: const EdgeInsets.all(4),
      padding: const EdgeInsets.symmetric(horizontal: 8),
      decoration: AppDecorations.card,
      child: Row(
        children: <Widget>[
          Container(
            width: 100,
            height: 100,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                image: DecorationImage(
                  image: NetworkImage(gig.photo.uri),
                  fit: BoxFit.cover,
                )),
          ),
          SizedBox(width: 15),
          Flexible(
            child: Column(
              // mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  gig.name,
                  style: AppStyles.header.copyWith(fontSize: 18),
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  gig.location.name,
                  style: AppStyles.primaryText,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                ),
                Text(
                  I18N.formatGigDate(gig.datetime),
                  style: AppStyles.secondaryText,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container _backPrintCard(Widget child) {
    return Container(
      height: 120,
      width: double.infinity,
      margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 10),
      decoration: AppDecorations.card,
      child: child,
    );
  }
}

class _ViewModel {
  final AppState _state;
  final Function _dispatch;

  _ViewModel(Store<AppState> store)
      : _state = store.state,
        _dispatch = store.dispatch;

  AsyncState<List<Gig>> get loadOpenGigsState =>
      _state.domainState.loadOpenGigsState;

  void loadOpenGigs() => _dispatch(new LoadOpenGigs());

  operator ==(o) {
    return o is _ViewModel && this.loadOpenGigsState == o.loadOpenGigsState;
  }

  @override
  int get hashCode => 0;
}
