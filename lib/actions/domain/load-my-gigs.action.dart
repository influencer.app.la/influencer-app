/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:redux_thunk/redux_thunk.dart';

import 'package:app/actions/actions.dart';
import 'package:app/models/domain.models.dart';
import 'package:app/store/app.store.dart';
import 'package:app/utils/async_state.dart';


class LoadMyGigsStateChanged {
  final AsyncState<List<Gig>> state;

  LoadMyGigsStateChanged(this.state);

  @override
  String toString() =>
      '$runtimeType.' +
      '${state.status}'.substring('.${state.status.runtimeType}'.length);
}

class LoadMyGigs implements AsyncAction {
  @override
  ThunkAction<AppState> execute(api) => (store) async {
        print('LoadMyGigs.execute()');

        final session = store.state.authState.loginState.value;
        if (session == null) return;

        store.dispatch(LoadMyGigsStateChanged(AsyncState.inProgress()));
        try {
          final list = await api.call('/gigs/self', session: session);
          final entity = (list as List).map((map) => Gig.fromJson(map)).toList();
          store.dispatch(LoadMyGigsStateChanged(AsyncState.success(entity)));
        } catch (error) {
          store.dispatch(LoadMyGigsStateChanged(AsyncState.failed(error)));
        }
      };
}
