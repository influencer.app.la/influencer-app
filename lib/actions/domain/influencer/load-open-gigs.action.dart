/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:app/models/domain.models.dart';
import 'package:app/store/app.store.dart';
import 'package:app/utils/async_state.dart';
import 'package:redux_thunk/redux_thunk.dart';

import '../../actions.dart';

class LoadOpenGigsStateChanged {
  final AsyncState<List<Gig>> state;

  LoadOpenGigsStateChanged(this.state);

  @override
  String toString() =>
      '$runtimeType.' +
      '${state.status}'.substring('.${state.status.runtimeType}'.length);
}

class LoadOpenGigs implements AsyncAction {
  @override
  ThunkAction<AppState> execute(api) => (store) async {
        print('LoadGigs.execute()');

        final session = store.state.authState.loginState.value;
        if (session == null) return;

        store.dispatch(LoadOpenGigsStateChanged(AsyncState.inProgress()));
        try {
          final list = await api
              .call('/gigs', session: session, params: {'status': 'OPEN'});
          final entity = (list as List).map((map) => Gig.fromJson(map)).toList();
          store.dispatch(LoadOpenGigsStateChanged(AsyncState.success(entity)));
        } catch (error) {
          store.dispatch(LoadOpenGigsStateChanged(AsyncState.failed(error)));
        }
      };
}
