/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance software development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:app/actions/api.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'store/app.store.dart';
import 'views/app.dart';

final api = new ApiClient();
final store = new AppStore(api);

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
    statusBarColor: Colors.transparent,
    statusBarIconBrightness: Brightness.dark,
  ));
  runApp(App(store));
}
