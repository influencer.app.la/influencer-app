/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */
import 'package:app/models/instagram.models.dart';
import 'package:app/views/components/notifications.dart';
import 'package:app/views/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:url_launcher/url_launcher.dart';

import '../headers.dart';

class InstagramProfileDetails extends StatelessWidget {
  final InstagramProfile profile;
  final bool showUsername;

  const InstagramProfileDetails({
    Key key,
    @required this.profile,
    this.showUsername = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        _profileHeader(profile),
        Padding(
          padding: const EdgeInsets.fromLTRB(22, 5, 15, 20),
          child: Text(profile.bio, style: AppStyles.secondaryText),
        ),
        _recentMedia(context, profile),
      ],
    );
  }

  Widget _recentMedia(BuildContext context, InstagramProfile profile) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        new SectionHeader('Recent Posts'),
        SizedBox(height: 5),
        SizedBox(
          height: 100,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: profile.recentMedia.length,
            itemBuilder: (_, index) {
              final media = profile.recentMedia[index];
              return InkWell(
                borderRadius: BorderRadius.circular(5),
                onTap: () {
                  canLaunch(media.link).then((can) {
                    if (!can) {
                      Notifications.showDanger(context,
                          message: 'Failed to open post');
                      return;
                    }
                    launch(media.link);
                  });
                },
                child: Container(
                  margin: EdgeInsets.all(5),
                  width: 90,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(media.images.thumbnail.url),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(5),
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _profileHeader(InstagramProfile profile) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Row(
        children: <Widget>[
          Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(profile.picture),
                fit: BoxFit.cover,
              ),
              shape: BoxShape.circle,
            ),
          ),
          SizedBox(width: 20),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(profile.fullname, style: AppStyles.header),
              if (showUsername)
                Text('@' + profile.username, style: AppStyles.primaryText),
              Padding(
                padding: const EdgeInsets.only(top: 10, right: 20),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    _stat('Posts', profile.mediaCount),
                    _stat('Followers', profile.followers),
                  ],
                ),
              )
            ],
          ))
        ],
      ),
    );
  }

  Column _stat(String header, Object value) {
    return Column(
      children: <Widget>[
        Text(header,
            style: AppStyles.primaryText.copyWith(fontWeight: FontWeight.bold)),
        Text(value.toString(), style: AppStyles.primaryText),
      ],
    );
  }
}
