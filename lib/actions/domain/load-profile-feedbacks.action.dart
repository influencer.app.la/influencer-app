/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */
import 'package:app/actions/actions.dart';
import 'package:app/models/domain.models.dart';
import 'package:app/store/app.store.dart';
import 'package:app/store/domain.store.dart';
import 'package:app/utils/async_state.dart';
import 'package:redux_thunk/redux_thunk.dart';

class LoadProfileFeedbacks implements AsyncAction {
  final Profile profile;

  LoadProfileFeedbacks(this.profile);

  @override
  ThunkAction<AppState> execute(api) => (store) async {
        print('LoadProfileFeedbacks.execute()');

        final session = store.state.authState.loginState.value;
        if (session == null) return;

        store.dispatch(ChangeLoadProfileFeedbacksState(
            AsyncState.inProgress(LoadProfileFeedbacksPayload(profile, null))));
        try {
          final params = {'receiver': profile.id};
          final json =
              await api.call('/feedbacks', params: params, session: session);
          final entities =
              (json as List).map((item) => Feedback.fromMap(item)).toList();
          store.dispatch(ChangeLoadProfileFeedbacksState(AsyncState.success(
              LoadProfileFeedbacksPayload(profile, entities))));
        } catch (error) {
          store.dispatch(ChangeLoadProfileFeedbacksState(AsyncState.failed(
              error, LoadProfileFeedbacksPayload(profile, null))));
        }
      };
}
