/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:app/actions/api.dart';
import 'package:app/models/domain.models.dart';
import 'package:app/store/app.store.dart';
import 'package:app/utils/async_state.dart';
import 'package:redux_thunk/redux_thunk.dart';

import '../../actions.dart';

class ApplyForGigStateChanged {
  final AsyncState<Gig> state;

  ApplyForGigStateChanged(this.state);

  @override
  String toString() =>
      '$runtimeType.' +
      '${state.status}'.substring('.${state.status.runtimeType}'.length);
}

class ApplyForGig implements AsyncAction {
  final Gig gig;

  ApplyForGig(this.gig);

  @override
  ThunkAction<AppState> execute(api) => (store) async {
        print('ApplyForGig.execute()');

        final session = store.state.authState.loginState.value;
        if (session == null) return;

        store.dispatch(ApplyForGigStateChanged(AsyncState.inProgress(gig)));
        try {
          final map = await api.call('/gigs/${gig.id}/applicants/self',
              method: HttpMethod.POST, session: session);
          final result = Gig.fromJson(map);
          store.dispatch(ApplyForGigStateChanged(AsyncState.success(result)));
        } catch (error) {
          store.dispatch(ApplyForGigStateChanged(AsyncState.failed(error)));
        }
      };
}
