/*
 * @author Oleg Khalidov (brooth@gmail.com).
 * -----------------------------------------------
 * Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import '../theme.dart';
import 'activity.dart';
import 'base.dart';

class TopBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final Widget titleWidget;
  final Widget leftAction;
  final Widget rightAction;
  final List<Widget> rightActions;
  final bool progress;
  final bool line;
  final double lineHeight;
  final Brightness brightness;
  final Color backgroundColor;

  const TopBar({
    Key key,
    this.title,
    this.titleWidget,
    this.leftAction,
    this.rightAction,
    this.rightActions,
    this.progress = false,
    this.line = false,
    this.lineHeight = AppDimensions.topBarLineHeight,
    this.brightness = Brightness.dark,
    this.backgroundColor = AppColors.topBarBg,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: <Widget>[
        AppBar(
          elevation: AppDimensions.topBarElevation,
          backgroundColor: backgroundColor,
          brightness: brightness,
          automaticallyImplyLeading: false,
          title: titleWidget ??
              Padding(
                padding: AppDimensions.topBarContentPadding,
                child: Text(title, style: AppStyles.topBarTitle),
              ),
          centerTitle: true,
          leading: leftAction,
          actions: rightActions != null
              ? rightActions
                  .map(
                    (action) => Padding(
                        padding: AppDimensions.topBarContentPadding,
                        child: action),
                  )
                  .toList()
              : <Widget>[
                  Padding(
                      padding: AppDimensions.topBarContentPadding,
                      child: rightAction),
                ],
        ),
        progress
            ? LinearActivityIndicator(
                height: lineHeight, color: AppColors.topBarLine)
            : line
                ? Separator(color: AppColors.topBarLine, height: lineHeight)
                : SizedBox(),
      ],
    );
  }

  @override
  Size get preferredSize => Size(double.infinity, AppDimensions.topBarHeight);

  TopBar copyWith({
    String title,
    Widget titleWidget,
    Widget leftAction,
    Widget rightAction,
    List<Widget> rightActions,
    bool progress,
    bool line,
    double lineHeight,
    Brightness brightness,
    Color backgroundColor,
  }) =>
      TopBar(
        title: title ?? this.title,
        titleWidget: titleWidget ?? this.titleWidget,
        leftAction: leftAction ?? this.leftAction,
        rightAction: rightAction ?? this.rightAction,
        rightActions: rightActions ?? this.rightActions,
        progress: progress ?? this.progress,
        line: line ?? this.line,
        lineHeight: lineHeight ?? this.lineHeight,
        brightness: brightness ?? this.brightness,
        backgroundColor: backgroundColor ?? this.backgroundColor,
      );
}

class TopBarMenuItem {
  final title;
  final Function onTap;
  final bool disabled;

  TopBarMenuItem(this.title, this.onTap, {this.disabled = false});
}

class TopBarMenuAction extends StatelessWidget {
  final List<TopBarMenuItem> items;
  final IconData icon;
  final Widget iconWidget;
  final bool disabled;

  const TopBarMenuAction({
    Key key,
    @required this.items,
    this.icon,
    this.iconWidget,
    this.disabled = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<TopBarMenuItem>(
      icon: Icon(icon ?? Icons.more_vert, color: AppColors.topBarIcon),
      child: iconWidget,
      enabled: !disabled,
      offset: Offset(0, 65),
      onSelected: (item) => item.onTap(),
      itemBuilder: (BuildContext context) {
        return items.map((item) {
          return PopupMenuItem<TopBarMenuItem>(
            value: item,
            enabled: !item.disabled,
            child: Text(
              item.title,
              style: AppStyles.actionText
                  .copyWith(color: AppColors.darkText, fontSize: 17),
            ),
          );
        }).toList();
      },
    );
  }
}

class TopBarAction extends StatelessWidget {
  final IconData icon;
  final Icon iconWidget;
  final GestureTapCallback onPress;
  final double size;
  final Color color;
  final EdgeInsetsGeometry padding;
  final bool progress;
  final bool disabled;

  const TopBarAction({
    Key key,
    this.icon,
    this.iconWidget,
    @required this.onPress,
    this.size = 22.0,
    this.color,
    this.padding = AppDimensions.topBarActionPadding,
    this.progress = false,
    this.disabled = false,
  }) : super(key: key);

  const TopBarAction.next(GestureTapCallback onPress)
      : this(
          icon: Icons.arrow_forward,
          padding: const EdgeInsets.fromLTRB(6.0, 6.0, 16.0, 0.0),
          onPress: onPress,
          size: 24,
        );

  const TopBarAction.back(GestureTapCallback onPress, {Color color})
      : this(
            icon: Icons.arrow_back,
            padding: const EdgeInsets.fromLTRB(6.0, 6.0, 12.0, 0.0),
            onPress: onPress,
            size: 24,
            color: color);

  const TopBarAction.cancel(GestureTapCallback onPress)
      : this(icon: Icons.close, onPress: onPress);

  const TopBarAction.save(GestureTapCallback onPress, {bool progress = false})
      : this(icon: Icons.check, onPress: onPress, progress: progress);

  const TopBarAction.delete(GestureTapCallback onPress, {bool progress = false})
      : this(
            icon: Icons.delete_outline,
            size: 22.0,
            padding: const EdgeInsets.fromLTRB(12.0, 5.0, 16.0, 2.0),
            onPress: onPress,
            progress: progress);

  const TopBarAction.settings(GestureTapCallback onPress)
      : this(icon: Icons.settings, size: 20.0, onPress: onPress);

  const TopBarAction.add(GestureTapCallback onPress)
      : this(
            icon: Icons.add,
            size: 25.0,
            padding: const EdgeInsets.fromLTRB(12.0, 6.0, 12.0, 0),
            onPress: onPress);

  const TopBarAction.edit(GestureTapCallback onPress)
      : this(icon: Icons.edit, size: 20.0, onPress: onPress);

  const TopBarAction.search(GestureTapCallback onPress, {bool progress = false})
      : this(icon: Icons.search, onPress: onPress, progress: progress);

  const TopBarAction.refresh(GestureTapCallback onPress,
      {bool progress = false})
      : this(icon: Icons.refresh, onPress: onPress, progress: progress);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: progress
          ? Padding(
              padding: EdgeInsets.symmetric(horizontal: 3.4),
              child: CircularActivityIndicator(
                  size: size * .7, color: AppColors.topBarIcon))
          : InkResponse(
              child: iconWidget ??
                  Icon(
                    icon,
                    color: disabled
                        ? AppColors.topBarIconDisabled
                        : color ?? AppColors.topBarIcon,
                    size: size,
                  ),
              radius: 23.0,
              highlightColor: Colors.transparent,
              onTap: disabled || progress ? null : onPress,
            ),
    );
  }
}

abstract class BottomBarTab {
  Widget icon();
  Widget text();
  Widget topBar(BuildContext context, TopBar base);
  Widget build(BuildContext context);
}

class BottomBar extends StatelessWidget {
  final List<BottomBarTab> tabs;
  final int activeTabIndex;
  final ValueChanged<int> onTap;

  const BottomBar({Key key, this.tabs, this.activeTabIndex, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: AppDecorations.bottomBar,
      child: Theme(
        data: Theme.of(context).copyWith(
            canvasColor: AppColors.bottomBarBg,
            primaryColor: AppColors.bottomBarItem,
            textTheme: Theme.of(context)
                .textTheme
                .copyWith(caption: AppStyles.bottomBarItemTitle)),
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          iconSize: 25.0,
          items: tabs
              .map(
                (tab) => BottomNavigationBarItem(
                  icon: Padding(
                    padding: AppDimensions.bottomBarIconPadding,
                    child: tab.icon(),
                  ),
                  title: tab.text(),
                ),
              )
              .toList(),
          currentIndex: activeTabIndex,
          onTap: this.onTap,
        ),
      ),
    );
  }
}
