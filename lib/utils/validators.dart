/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

final RegExp usernameRegExp = RegExp(r"^[a-zA-Z_][a-zA-Z0-9_]*$");

final RegExp emailRegExp = RegExp(
    r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");

String validateUsername(String val) =>
    usernameRegExp.firstMatch(val) == null ? 'Invalid username' : null;

String validateEmail(String val) =>
    emailRegExp.firstMatch(val.toLowerCase()) == null ? 'Invalid email' : null;

String validatePassword(String val) =>
    val.length < 3 ? 'Password too short' : null;
