/*
 * @author Oleg Khalidov <brooth@gmail.com>.
 * -----------------------------------------------
 * Freelance software development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:app/actions/auth/login.action.dart';
import 'package:app/actions/domain/lookup-location.action.dart';
import 'package:app/actions/domain/influencer/apply-for-gig.action.dart';
import 'package:app/actions/domain/influencer/load-open-gigs.action.dart';
import 'package:app/actions/domain/influencer/quit-gig.action.dart';
import 'package:app/actions/domain/load-my-gigs.action.dart';
import 'package:app/models/domain.models.dart';
import 'package:app/store/app.store.dart';
import 'package:app/utils/async_state.dart';
import 'package:app/utils/store.utils.dart';

class ChangeCreateGigState extends ChangeAsyncStateAction<Gig> {
  ChangeCreateGigState(AsyncState<Gig> state) : super(state);
}

class ChangeDeleteGigState extends ChangeAsyncStateAction<String> {
  ChangeDeleteGigState(AsyncState<String> state) : super(state);
}

class ChangeStartGigState extends ChangeAsyncStateAction<Gig> {
  ChangeStartGigState(AsyncState<Gig> state) : super(state);
}

class ChangeCloseGigState extends ChangeAsyncStateAction<Gig> {
  ChangeCloseGigState(AsyncState<Gig> state) : super(state);
}

class ChangeCancelGigState extends ChangeAsyncStateAction<Gig> {
  ChangeCancelGigState(AsyncState<Gig> state) : super(state);
}

class ChangeLoadProfileFeedbacksState
    extends ChangeAsyncStateAction<LoadProfileFeedbacksPayload> {
  ChangeLoadProfileFeedbacksState(AsyncState<LoadProfileFeedbacksPayload> state)
      : super(state);
}

class LoadProfileFeedbacksPayload {
  final Profile profile;
  final List<Feedback> feedbacks;

  LoadProfileFeedbacksPayload(this.profile, this.feedbacks);
}

class DomainState {
  final AsyncState<List<Gig>> loadOpenGigsState;
  final AsyncState<Gig> applyForGigState;
  final AsyncState<Gig> quitGigState;
  final AsyncState<List<Gig>> loadMyGigsState;
  final AsyncState<Gig> createGigState;
  final AsyncState<LookupLocationValue> lookupLocationState;
  final AsyncState<String> deleteGig;
  final AsyncState<Gig> cancelGig;
  final AsyncState<Gig> closeGig;
  final AsyncState<Gig> startGig;
  final AsyncState<LoadProfileFeedbacksPayload> loadProfileFeedbacks;

  DomainState(
    this.loadOpenGigsState,
    this.applyForGigState,
    this.quitGigState,
    this.loadMyGigsState,
    this.createGigState,
    this.lookupLocationState,
    this.deleteGig,
    this.cancelGig,
    this.closeGig,
    this.startGig,
    this.loadProfileFeedbacks,
  );

  DomainState.initial()
      : this(
          AsyncState.empty(),
          AsyncState.empty(),
          AsyncState.empty(),
          AsyncState.empty(),
          AsyncState.empty(),
          AsyncState.empty(),
          AsyncState.empty(),
          AsyncState.empty(),
          AsyncState.empty(),
          AsyncState.empty(),
          AsyncState.empty(),
        );

  DomainState copyWith({
    AsyncState<List<Gig>> loadOpenGigsState,
    AsyncState<Gig> applyForGigState,
    AsyncState<Gig> quitGigState,
    AsyncState<List<Gig>> loadMyGigsState,
    AsyncState<Gig> createGigState,
    AsyncState<LookupLocationValue> lookupLocationState,
    AsyncState<String> deleteGig,
    AsyncState<Gig> cancelGig,
    AsyncState<Gig> startGig,
    AsyncState<Gig> closeGig,
    AsyncState<LoadProfileFeedbacksPayload> loadProfileFeedbacks,
  }) =>
      DomainState(
        loadOpenGigsState ?? this.loadOpenGigsState,
        applyForGigState ?? this.applyForGigState,
        quitGigState ?? this.quitGigState,
        loadMyGigsState ?? this.loadMyGigsState,
        createGigState ?? this.createGigState,
        lookupLocationState ?? this.lookupLocationState,
        deleteGig ?? this.deleteGig,
        cancelGig ?? this.cancelGig,
        closeGig ?? this.closeGig,
        startGig ?? this.startGig,
        loadProfileFeedbacks ?? this.loadProfileFeedbacks,
      );
}

DomainState domainReducer(AppState state, dynamic action) {
  if (action is LoginStateChanged && action.state.isNotSuccessful)
    return DomainState.initial();

  if (action is ChangeLoadProfileFeedbacksState) {
    return state.domainState.copyWith(loadProfileFeedbacks: action.state);
  }

  if (action is ChangeCreateGigState) {
    return state.domainState.copyWith(
        createGigState: action.state,
        loadMyGigsState: action.state.isSuccessful &&
                state.domainState.loadMyGigsState.hasValue
            ? state.domainState.loadMyGigsState.copyWith(
                value: List.from(state.domainState.loadMyGigsState.value)
                  ..add(action.state.value))
            : state.domainState.loadMyGigsState);
  }
  if (action is ChangeDeleteGigState) {
    return state.domainState.copyWith(
        deleteGig: action.state,
        loadMyGigsState: action.state.isSuccessful &&
                state.domainState.loadMyGigsState.hasValue
            ? state.domainState.loadMyGigsState.copyWith(
                value: state.domainState.loadMyGigsState.value
                    .where((g) => g.id != action.state.value)
                    .toList())
            : state.domainState.loadMyGigsState);
  }
  if (action is ChangeStartGigState) {
    return state.domainState.copyWith(
        startGig: action.state,
        loadMyGigsState: action.state.isSuccessful &&
                state.domainState.loadMyGigsState.hasValue
            ? state.domainState.loadMyGigsState.copyWith(
                value: state.domainState.loadMyGigsState.value
                    .map((item) => item.id == action.state.value.id
                        ? action.state.value
                        : item)
                    .toList())
            : state.domainState.loadMyGigsState);
  }
  if (action is ChangeCloseGigState) {
    return state.domainState.copyWith(
        closeGig: action.state,
        loadMyGigsState: action.state.isSuccessful &&
                state.domainState.loadMyGigsState.hasValue
            ? state.domainState.loadMyGigsState.copyWith(
                value: state.domainState.loadMyGigsState.value
                    .map((item) => item.id == action.state.value.id
                        ? action.state.value
                        : item)
                    .toList())
            : state.domainState.loadMyGigsState);
  }
  if (action is ChangeCancelGigState) {
    return state.domainState.copyWith(
        cancelGig: action.state,
        loadMyGigsState: action.state.isSuccessful &&
                state.domainState.loadMyGigsState.hasValue
            ? state.domainState.loadMyGigsState.copyWith(
                value: state.domainState.loadMyGigsState.value
                    .map((item) => item.id == action.state.value.id
                        ? action.state.value
                        : item)
                    .toList())
            : state.domainState.loadMyGigsState);
  }

  if (action is ChangeCloseGigState) {
    return state.domainState.copyWith(
        closeGig: action.state,
        loadMyGigsState: action.state.isSuccessful &&
                state.domainState.loadMyGigsState.hasValue
            ? state.domainState.loadMyGigsState.copyWith(
                value: state.domainState.loadMyGigsState.value
                    .map((item) => item.id == action.state.value.id
                        ? action.state.value
                        : item)
                    .toList())
            : state.domainState.loadMyGigsState);
  }

  if (action is LookupLocationStateChanged) {
    return state.domainState.copyWith(lookupLocationState: action.state);
  }

  if (action is LoadMyGigsStateChanged) {
    return state.domainState.copyWith(loadMyGigsState: action.state);
  }

  if (action is LoadOpenGigsStateChanged) {
    return state.domainState.copyWith(loadOpenGigsState: action.state);
  }

  if (action is ApplyForGigStateChanged) {
    var loadGigsState = state.domainState.loadOpenGigsState;
    return state.domainState.copyWith(
        applyForGigState: action.state,
        loadOpenGigsState: action.state.isSuccessful && loadGigsState.hasValue
            ? loadGigsState.copyWith(
                value: loadGigsState.value
                    .map((v) =>
                        v.id == action.state.value.id ? action.state.value : v)
                    .toList())
            : loadGigsState);
  }

  if (action is QuitGigStateChanged) {
    var loadGigsState = state.domainState.loadOpenGigsState;
    return state.domainState.copyWith(
        quitGigState: action.state,
        loadOpenGigsState: action.state.isSuccessful && loadGigsState.hasValue
            ? loadGigsState.copyWith(
                value: loadGigsState.value
                    .map((v) =>
                        v.id == action.state.value.id ? action.state.value : v)
                    .toList())
            : loadGigsState);
  }

  return state.domainState;
}
