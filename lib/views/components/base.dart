/*
 * @author Oleg Khalidov (brooth@gmail.com).
 * -----------------------------------------------
 * Software Development:
 * Upwork: https://www.upwork.com/fl/khalidovoleg
 */

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:app/utils/types.dart';

import '../theme.dart';

class InputFieldController {
  String error;
  String success;

  final FocusNode focusNode = FocusNode();
  final TextEditingController editingController;
  final Validator validator;
  final String requiredError;
  final String initialValue;

  InputFieldController({this.validator, this.requiredError, this.initialValue})
      : editingController = TextEditingController(text: initialValue);

  String get value => editingController.text;

  set value(String text) => editingController.text = text ?? '';

  String validate(BuildContext context) {
    if (error == null) {
      if (requiredError != null && value.isEmpty)
        error = requiredError;
      else if (validator != null) error = validator(value);
    }
    if (error != null) focus(context);
    return error;
  }

  focus(BuildContext context) {
    FocusScope.of(context).requestFocus(focusNode);
  }
}

class KeyboardSpace extends StatelessWidget {
  final double heightRatio;

  KeyboardSpace({this.heightRatio = 1.0});

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return AnimatedContainer(
      duration: const Duration(milliseconds: 120),
      margin:
          EdgeInsets.only(bottom: mediaQuery.viewInsets.vertical * heightRatio),
    );
  }
}

class Separator extends StatelessWidget {
  final Color color;
  final double height;

  Separator({this.color, this.height = 1.3});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: height,
      color: color ?? AppColors.separator,
    );
  }
}

class CircleIconButton extends StatelessWidget {
  final IconData icon;
  final Color color;
  final GestureTapCallback onPress;
  final double size;
  final double iconSize;
  final double borderWidth;
  final EdgeInsetsGeometry padding;
  final bool innerMaterial;

  CircleIconButton({
    @required this.icon,
    @required this.onPress,
    this.color = Colors.white,
    this.size = 25.0,
    this.iconSize,
    this.borderWidth = 1.0,
    this.padding,
    this.innerMaterial = false,
  });

  @override
  Widget build(BuildContext context) {
    final widget = Container(
      padding: padding,
      child: InkResponse(
        splashColor: color.withOpacity(0.5),
        highlightColor: Colors.transparent,
        radius: size,
        onTap: onPress,
        child: Container(
          width: size,
          height: size,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                color: color,
                width: borderWidth,
              )),
          child: Icon(icon, color: color, size: iconSize ?? size - 6),
        ),
      ),
    );
    return innerMaterial
        ? Material(
            type: MaterialType.transparency,
            borderRadius: BorderRadius.circular(size),
            child: widget,
          )
        : widget;
  }
}
